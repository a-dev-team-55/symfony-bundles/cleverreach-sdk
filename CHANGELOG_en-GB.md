# 0.1.0
* Bundle basic structure created

# 0.1.1
* Endpoints and Methods added
* 
# 0.1.2
* WebHook Api Client added
* Added missing properties to ReceiverStruct

# 0.1.3
* Coderefactoring
* Readme created

# 1.0.2
* Requirements adjusted

# 1.0.3
* Guzzle Streamfactory Error fixed
