# Clever Reach Bundle
**Package Name:** `a-dev-team/clever-reach-bundle`<br>
**Requirements:** `PHP 8.1`<br>

## Installation
```shell
composer config repositories.gitlab.com/78563314 '{"branch": "main", "type": "composer", "url": "https://gitlab.com/api/v4/group/78563314/-/packages/composer/packages.json"}'
```

```shell
composer require a-dev-team/clever-reach-bundle
```

## Benutzung
### Service injecten
*XML*
```xml
<services>
    <service id="Your\Service\Class">
        <argument type="service" id="clever_reach.http_client"/>
    </service>
</services>
```

*YAML*
```yaml
Your\Service\Class:
    arguments: [
        '@clever_reach.http_client'
    ]
```

### Constructor
```php
/**
 * @param CleverReachClient $client
 */
public function __construct(private readonly CleverReachClient $client) {}
```

### Authentifizierung
```php
$this->client
    ->setClientId($clientId)
    ->setClientSecret($clientSecret)
    ->authenticate();
```

### Aufruf
```php
// Example
$groups = $this->client->groups()->getAll();
```


# Endpunkte und Methoden
## Gruppen
<details>
  <summary>getAll</summary>

Gibt alle Gruppen zurück
```php
/**
 * @return array
 */
public function getAll(): array
```
</details>

<details>
  <summary>getOneByGroupId</summary>

Gibt eine Gruppe zurück
```php
/**
 * @param string $groupId
 * @return array
 */
public function getOneByGroupId(string $groupId): array
```
</details>

<details>
  <summary>getFormsByGroupId</summary>

Gibt alle Gruppeformulare einer Gruppe zurück
```php
/**
 * @param string $groupId
 * @return array
 */
public function getFormsByGroupId(string $groupId): array
```
</details>

<details>
  <summary>getReceiversByGroupId</summary>

Gibt eine bestimmte Seite der Newsletterempfänger, anhand von `$page` und `$pagesize`, einer Gruppe zurück.<br>
Dabei werden nicht alle Empfängerdaten zurückgegeben. Um alle Empfängerdaten zu erhalten, müssen die Empfänger einzeln abgerufen werden.
```php
/**
 * @param string $groupId
 * @param int $page
 * @param int $pagesize
 * @return array
 */
public function getReceiversByGroupId(string $groupId, int $page = 0, int $pagesize = 50): array
```
</details>

<details>
  <summary>getAllReceiversByGroupId</summary>

Gibt alle Newsletterempfänger einer Gruppe zurück. Dabei wird ein Batchaufruf gestartet.
Die Anzahl der Requests hängt dabei von der Anzahl der Empfänger innerhalb der gewünschten Gruppe
und der Batchsize ab.
```php
/**
 * @param string $groupId
 * @param int|null $batchSize
 * @return array
 */
public function getAllReceiversByGroupId(string $groupId, int $batchSize = null): array
```
</details>

<details>
  <summary>getStatsByGroupId</summary>

Gibt die Statistik der gewünschten Gruppe zurück
```php
/**
 * @param string $groupId
 * @return array
 */
public function getStatsByGroupId(string $groupId): array
```
</details>

<details>
  <summary>getTotalCountByGroupId</summary>

Gibt die Gesamtanzahl aller Newsletterempfänger einer Gruppe zurück
```php
/**
 * @param string $groupId
 * @return int
 */
public function getTotalCountByGroupId(string $groupId): int
```
</details>

<details>
  <summary>getActiveCountByGroupId</summary>

Gibt die Anzahl aller aktiven Newsletterempfänger einer Gruppe zurück
```php
/**
 * @param string $groupId
 * @return int
 */
public function getActiveCountByGroupId(string $groupId): int
```
</details>

<details>
  <summary>getInactiveCountByGroupId</summary>

Gibt die Anzahl aller inaktiven Newsletterempfänger einer Gruppe zurück
```php
/**
 * @param string $groupId
 * @return int
 */
public function getInactiveCountByGroupId(string $groupId): int
```
</details>

<details>
  <summary>getBounceCountByGroupId</summary>

Gibt die Anzahl aller gebouncten Newsletterempfänger einer Gruppe zurück
```php
/**
 * @param string $groupId
 * @return int
 */
public function getBounceCountByGroupId(string $groupId): int
```
</details>

<details>
  <summary>addReceiver</summary>

Fügt einen Newsletterempfänger zu einer Gruppe hinzu
```php
/**
 * @param string $groupId
 * @param ReceiverStruct $receiver
 * @return array
 */
public function addReceiver(string $groupId, ReceiverStruct $receiver): array
```
</details>

<details>
  <summary>updateReceiver</summary>

Updatet einen Newsletterempfänger in einer Gruppe
```php
/**
 * @param string $groupId
 * @param string $receiverIdOrMail
 * @param ReceiverStruct $receiver
 * @return array
 */
public function updateReceiver(string $groupId, string $receiverIdOrMail, ReceiverStruct $receiver): array
```
</details>

<details>
  <summary>activateReceiver</summary>

Aktiviert einen Newsletterempfänger
```php
/**
 * @param string $groupId
 * @param string $receiverIdOrMail
 * @return array
 */
public function activateReceiver(string $groupId, string $receiverIdOrMail): array
```
</details>

<details>
  <summary>deactivateReceiver</summary>

Deaktiviert einen Newsletterempfänger
```php
/**
 * @param string $groupId
 * @param string $receiverIdOrMail
 * @return array
 */
public function deactivateReceiver(string $groupId, string $receiverIdOrMail): array
```
</details>

<details>
  <summary>addReceivers</summary>

Fügt mehrere Newsletterempfänger zu einer Gruppe hinzu
```php
/**
 * @param string $groupId
 * @param ReceiverStruct[] $receivers
 * @return array
 */
public function addReceivers(string $groupId, array $receivers): array
```
</details>

<details>
  <summary>getFiltersByGroupId</summary>

Gibt die Filter(Segmente) einer Gruppe zurück
```php
/**
 * @param string $groupId
 * @return array
 */
public function getFiltersByGroupId(string $groupId): array
```
</details>

## Formulare
<details>
  <summary>getAll</summary>

Gibt alle Formulare zurück
```php
/**
 * @return array
 */
public function getAll(): array
```
</details>

## Empfänger
<details>
  <summary>getOne</summary>

Gibt einen Newsletterempfänger zurück
```php
/**
 * @param string $groupId
 * @param string $receiverIdOrMail
 * @return array
 */
public function getOne(string $groupId, string $receiverIdOrMail): array
```
</details>

## Hooks
<details>
  <summary>register</summary>

Registriert einen Webhook
```php
/**
 * @param string $url
 * @param string $groupId
 * @param HookEvent $event
 * @return array
 */
public function register(string $url, string $groupId, HookEvent $event): array
```
</details>

<details>
  <summary>getAll</summary>

Gibt eine Liste aller registrierten Webhooks zurück
```php
/**
 * @return array
 */
public function getAll(): array
```
</details>

<details>
  <summary>delete</summary>

Löscht einen Webhook
```php
/**
 * @param string $groupId
 * @param HookEvent $event
 * @return array
 */
public function delete(string $groupId, HookEvent $event): array
```
</details>

# Commands
## V3
### cr:v3:form:get-all              
CleverReach V3 - Get all Forms

### cr:v3:group:active-receivers    
CleverReach V3 - Get active receiver count by group ID

### cr:v3:group:add-receiver        
CleverReach V3 - Add group receiver

### cr:v3:group:bounced-receivers   
CleverReach V3 - Get bounced receiver count by group ID

### cr:v3:group:get-all             
CleverReach V3 - Get all Groups

### cr:v3:group:get-filters         
CleverReach V3 - Get group Filters by group ID

### cr:v3:group:get-forms           
CleverReach V3 - Get all forms by group ID

### cr:v3:group:get-one             
CleverReach V3 - Get one group by ID

### cr:v3:group:get-receivers       
CleverReach V3 - Get Group receivers by group ID

### cr:v3:group:get-stats           
CleverReach V3 - Get group Statistics by group ID

### cr:v3:group:inactive-receivers  
CleverReach V3 - Get inactive receiver count by group ID

### cr:v3:group:total-receivers     
CleverReach V3 - Get total receiver count by group ID

### cr:v3:receiver:get-one          
CleverReach V3 - Get one receiver by groupID and receiverId order receiverMail

# Dokumentation
## [Rest API Explorer - V2](https://rest.cleverreach.com/explorer/v2/) 
## [Rest API Explorer - V3](https://rest.cleverreach.com/explorer/v3/)
