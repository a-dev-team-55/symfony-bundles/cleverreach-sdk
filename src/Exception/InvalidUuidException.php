<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:38
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Exception;

use Exception;

/**
 * @class InvalidUuidException
 * @package ADevTeam\CleverReachBundle\Exception
 */
class InvalidUuidException extends Exception
{

    /**
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        parent::__construct(str_replace('{{ input }}', $uuid, 'Value is not a valid UUID: {{ input }}'));
    }
}
