<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:38
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Exception;

use Exception;

/**
 * @class InvalidUuidLengthException
 * @package ADevTeam\CleverReachBundle\Exception
 */
class InvalidUuidLengthException extends Exception
{

    /**
     * @param int $length
     * @param string $hex
     */
    public function __construct(int $length, string $hex)
    {
        parent::__construct(
            str_replace(
                ['{{ length }}', '{{ hex }}'],
                [$length, $hex],
                'UUID has a invalid length. 16 bytes expected, {{ length }} given. Hexadecimal reprensentation: {{ hex }}'
            )
        );
    }
}
