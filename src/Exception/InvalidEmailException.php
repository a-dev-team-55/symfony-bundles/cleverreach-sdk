<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:38
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Exception;

use Exception;

/**
 * @class InvalidEmailException
 * @package ADevTeam\CleverReachBundle\Exception
 */
class InvalidEmailException extends Exception
{

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        parent::__construct(str_replace('{{ email }}', $email, 'This is not a valid Email-Address: {{ email }}'));
    }
}
