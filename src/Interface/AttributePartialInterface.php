<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:09
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Struct\AttributeStruct;
use ADevTeam\CleverReachBundle\Struct\UpdateAttributeStruct;

interface AttributePartialInterface
{

    /**
     * @param string|null $groupId
     * @return CleverReachResponseInterface
     */
    public function getAll(?string $groupId = null): CleverReachResponseInterface;

    /**
     * @param string $attributeId
     * @return CleverReachResponseInterface
     */
    public function getOne(string $attributeId): CleverReachResponseInterface;

    /**
     * @param AttributeStruct $attribute
     * @return CleverReachResponseInterface
     */
    public function create(AttributeStruct $attribute): CleverReachResponseInterface;

    /**
     * @param UpdateAttributeStruct $attribute
     * @return CleverReachResponseInterface
     */
    public function update(UpdateAttributeStruct $attribute): CleverReachResponseInterface;

    /**
     * @param string $attributeId
     * @return CleverReachResponseInterface
     */
    public function delete(string $attributeId): CleverReachResponseInterface;
}
