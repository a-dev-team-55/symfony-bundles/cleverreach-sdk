<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:12
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Struct\BlacklistedEmail;
use ADevTeam\CleverReachBundle\Struct\ReceiverValidationStruct;

interface BlacklistPartialInterface
{

    /**
     * @return CleverReachResponseInterface
     */
    public function getAll(): CleverReachResponseInterface;

    /**
     * @param string $email
     * @return CleverReachResponseInterface
     */
    public function getOneByMail(string $email): CleverReachResponseInterface;

    /**
     * @param BlacklistedEmail $blacklistedEmail
     * @return CleverReachResponseInterface
     */
    public function add(BlacklistedEmail $blacklistedEmail): CleverReachResponseInterface;

    /**
     * @param ReceiverValidationStruct $struct
     * @return CleverReachResponseInterface
     */
    public function getNotBlacklistedByGivenMails(ReceiverValidationStruct $struct): CleverReachResponseInterface;

    /**
     * @param BlacklistedEmail $blacklistedEmail
     * @return CleverReachResponseInterface
     */
    public function update(BlacklistedEmail $blacklistedEmail): CleverReachResponseInterface;

    /**
     * @param string $email
     * @return CleverReachResponseInterface
     */
    public function delete(string $email): CleverReachResponseInterface;
}
