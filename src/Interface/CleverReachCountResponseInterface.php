<?php
/*
 * @author ADevTeam
 * @created 26.11.2023 17:55
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use Psr\Http\Message\ResponseInterface;

interface CleverReachCountResponseInterface extends ResponseInterface
{

    /**
     * @return mixed
     */
    public function getValue(): mixed;
}
