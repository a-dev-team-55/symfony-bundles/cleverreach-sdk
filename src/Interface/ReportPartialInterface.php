<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:42
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Constant\ReportState;
use ADevTeam\CleverReachBundle\Constant\StatisticMode;
use DateTimeInterface;

interface ReportPartialInterface
{

    /**
     * @return CleverReachResponseInterface
     */
    public function getAll(): CleverReachResponseInterface;

    /**
     * @param string $reportIdOrMailingId
     * @return CleverReachResponseInterface
     */
    public function getOneByGroupId(string $reportIdOrMailingId): CleverReachResponseInterface;

    /**
     * @param string $reportIdOrMailingId
     * @param ReportState $state
     * @return CleverReachResponseInterface
     */
    public function getReceiverInfos(string $reportIdOrMailingId, ReportState $state = ReportState::SENT): CleverReachResponseInterface;

    /**
     * @param string $reportIdOrMailingId
     * @param StatisticMode $mode
     * @param DateTimeInterface|null $start
     * @param DateTimeInterface|null $end
     * @return CleverReachResponseInterface
     */
    public function getStatsByReportId(
        string $reportIdOrMailingId,                // ID of Report/Mailing.
        StatisticMode $mode = StatisticMode::FULL,  // Mode of statistics.
        ?DateTimeInterface $start = null,           // Startdate unixtimestamp (only for mode daily).
        ?DateTimeInterface $end = null              // Enddate unixtimestamp (only for mode daily).
    ): CleverReachResponseInterface;

    /**
     * Delete a certain report
     *
     * @param string $reportIdOrMailingId
     * @return CleverReachResponseInterface
     */
    public function delete(string $reportIdOrMailingId): CleverReachResponseInterface;
}
