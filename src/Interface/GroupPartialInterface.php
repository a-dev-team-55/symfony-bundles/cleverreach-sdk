<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:23
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Constant\OrderDirection;
use ADevTeam\CleverReachBundle\Http\CleverReachCountResponse;
use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Struct\AttributeStruct;
use ADevTeam\CleverReachBundle\Struct\EventStruct;
use ADevTeam\CleverReachBundle\Struct\FilterStruct;
use ADevTeam\CleverReachBundle\Struct\GroupStruct;
use ADevTeam\CleverReachBundle\Struct\OrderStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverList;
use ADevTeam\CleverReachBundle\Struct\ReceiverPagination;
use ADevTeam\CleverReachBundle\Struct\ReceiverStruct;
use ADevTeam\CleverReachBundle\Struct\UpdateAttributeStruct;
use Throwable;

interface GroupPartialInterface extends PartialInterface
{

    /**
     * returns array of groups (receiver lists)
     *
     * @param OrderDirection $order
     * @return CleverReachResponseInterface
     */
    public function getAll(OrderDirection $order = OrderDirection::DESC): CleverReachResponseInterface;

    /**
     * return extended stats about the group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     */
    public function getExtendedStats(string $groupId): CleverReachResponseInterface;

    /**
     * return local attributes.
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     */
    public function getGroupAttributes(string $groupId): CleverReachResponseInterface;

    /**
     * returns a list with all blacklisted email addresse for a group
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     */
    public function getBlacklist(string $groupId): CleverReachResponseInterface;

    /**
     * return filters
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     */
    public function getFilters(string $groupId): CleverReachResponseInterface;

    /**
     * return groups statistics based on a Filter
     *
     * @param string $groupId
     * @param string $filterId
     * @return CleverReachResponseInterface
     */
    public function getCountStatisticBasedOnFilter(string $groupId, string $filterId): CleverReachResponseInterface;

    /**
     * return a list of receivers
     *
     * @param string $groupId
     * @param string $filterId
     * @param ReceiverPagination $receiverPagination
     * @return CleverReachResponseInterface
     */
    public function getReceiversBasedOnFilter(string $groupId, string $filterId, ReceiverPagination $receiverPagination): CleverReachResponseInterface;

    /**
     * @param string $groupId
     * @param int|null $batchSize
     * @return array
     * @throws Throwable
     */
    public function getAllReceiversByGroupId(string $groupId, int $batchSize = null): array;

    /**
     * return groups statistics based on a Filter
     *
     * @param string $groupId
     * @param string $filterId
     * @return CleverReachResponseInterface
     */
    public function getStatisticBasedOnFilter(string $groupId, string $filterId): CleverReachResponseInterface;

    /**
     * return a list of receivers
     *
     * @param string $groupId
     * @param ReceiverPagination $receiverPagination
     * @return CleverReachResponseInterface
     */
    public function getReceivers(string $groupId, ReceiverPagination $receiverPagination): CleverReachResponseInterface;

    /**
     * return specific receiver of a certain group
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     */
    public function getReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * return a list of events for a receiver
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     */
    public function getReceiverEvents(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * return a list of orders
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     */
    public function getReceiverOrders(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * return group by given id
     *
     * @param string $groupId
     * @return CleverReachResponse
     */
    public function getOne(string $groupId): CleverReachResponseInterface;

    /**
     * return group forms
     *
     * @param string $groupId
     * @return CleverReachResponse
     */
    public function getForms(string $groupId): CleverReachResponseInterface;

    /**
     * return stats about the group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponse
     */
    public function getStats(string $groupId): CleverReachResponseInterface;

    /**
     * create a list of receivers
     *
     * Example Post Data:
     * {
     *   "name": "Justice League",
     *   "receiver_info": "Superhero colleagues",
     *   "locked": false,
     *   "backup": true
     * }
     *
     * @param GroupStruct $group
     * @return CleverReachResponse
     */
    public function create(GroupStruct $group): CleverReachResponseInterface;

    /**
     * create attribute
     *
     * Example Post Data:
     * {
     *   "name": "secret_id",
     *   "type": "text",
     *   "description": "Secret identity",
     *   "preview_value": "real name",
     *   "default_value": "Bruce Wayne"
     * }
     *
     * @param string $groupId
     * @param AttributeStruct $attribute
     * @return CleverReachResponse
     */
    public function createAttribute(string $groupId, AttributeStruct $attribute): CleverReachResponseInterface;

    /**
     * add an email address to group blacklist
     *
     * @param string $groupId
     * @param string $email
     * @return CleverReachResponse
     */
    public function addMailToGroupBlacklist(string $groupId, string $email): CleverReachResponseInterface;

    /**
     * creates a filter/segment
     *
     * Example Post Data:
     * {
     *   "name": "My Filter",
     *   "operator": "OR",
     *   "rules": [
     *     { "field":"email", "logic":"contains", "condition":"@gotham.com" },
     *     { "field":"activated", "logic":"bg", "condition":"1234567" },
     *     { "field":"firstname", "logic":"eq", "condition":"bruce" },
     *     { "field":"lastname", "logic":"eq", "condition":"wayne" }
     *   ]
     * }
     *
     * Possible Operators
     * OR
     * AND (default)
     *
     * Available Logics:
     * eq - equals
     * neq - not equals
     * sm - smaller
     * bg - bigger
     * isnull - is empty
     * notisnull - not empty
     * contains - contains given string
     * begins - begins with
     * ends - ends with
     * ismale - given field is male
     * isfemale - given field is female
     *
     * general available fields:
     * email - (email of the receiver)
     * source - (import source of the email)
     * imported - (timestamp of receiver import)
     * registered - (timestamp of receiver registration)
     * activated - (timestamp of receiver activation)
     * deactivated - (timestamp of receiver deactivation)
     * bounced - (timestamp of receiver bounce)
     * points - (points of the receiver)
     * last_location - (last location of receiver)
     * last_client - (last mail client used by receiver)
     * crevent_mail_send (logic: eq, condition: mailing_id -> receiver of certain mailing)
     * crevent_mail_open (logic: eq, condition: mailing_id -> opened mail of certain mailing)
     * crevent_mail_click (logic: eq, condition: mailing_id -> clicked on mail of certain mailing)
     * crevent_mail_bounce (logic: eq, condition: mailing_id -> bounced mail of certain mailing)
     * all attributes (if global and group attributes have the same name, group ones will be favoured)
     *
     * @param string $groupId
     * @param FilterStruct $filter
     * @return CleverReachResponse
     */
    public function createFilter(string $groupId, FilterStruct $filter): CleverReachResponseInterface;

    /**
     * create a new receiver
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * {
     *   "email":"bruce@gotham.com",
     *   "registered": "1118354400",
     *   "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *   "deactivated": "0", // omit field unless you know what you are doing!
     *   "source":"Batcave Computer",
     *   "attributes": {
     *     "is_batman":"1"
     *   },
     *   "global_attributes": {
     *     "firstname":"Bruce",
     *     "lastname":"Wayne"
     *   },
     *   "tags": [ "hero", "rich", "smart" ],
     *   "orders": [
     *     {
     *       "order_id": "xyz123412",
     *       "product_id": "SN9876543",
     *       "product": "Batman - The Movie (DVD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     },
     *     {
     *       "order_id": "xyz123411",
     *       "product_id": "SN9876542",
     *       "product": "Batman - The Soundtrack (CD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     }
     *   ]
     * }
     *
     * @param string $groupId
     * @param ReceiverStruct $receiver
     * @return CleverReachResponse
     */
    public function createReceiver(string $groupId, ReceiverStruct $receiver): CleverReachResponseInterface;

    /**
     * Delete receivers by given ids or emails.
     *
     * Example Post Data:
     * [
     *   "381940",
     *   "dick.grayson@wayne.com",
     *   "robin@gotham.com",
     *   "3571983"
     * ]
     *
     * @param string $groupId
     * @param ReceiverList $receivers
     * @return CleverReachResponse
     */
    public function deleteMultipleReceivers(string $groupId, ReceiverList $receivers): CleverReachResponseInterface;

    /**
     * create multiple receivers
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "registered": "1118354400",
     *     "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *     "deactivated": "0", // omit field unless you know what you are doing!
     *     "source":"Batcave Computer",
     *     "attributes": {
     *       "is_batman":"1"
     *     },
     *     "global_attributes": {
     *       "firstname":"Bruce",
     *       "lastname":"Wayne"
     *     },
     *     "tags": [ "hero", "rich", "smart" ],
     *     "orders": [
     *       {
     *         "order_id": "xyz123412",
     *         "product_id": "SN9876543",
     *         "product": "Batman - The Movie (DVD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       },
     *       {
     *         "order_id": "xyz123411",
     *         "product_id": "SN9876542",
     *         "product": "Batman - The Soundtrack (CD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       }
     *     ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponse
     */
    public function createMultipleReceivers(string $groupId, array $receivers): CleverReachResponseInterface;

    /**
     * Updates receivers or creates new ones, if email not existing
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "registered": "1118354400",
     *     "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *     "deactivated": "0", // omit field unless you know what you are doing!
     *     "source":"Batcave Computer",
     *     "attributes": {
     *       "is_batman":"1"
     *     },
     *     "global_attributes": {
     *       "firstname":"Bruce",
     *       "lastname":"Wayne"
     *     },
     *     "tags": [ "hero", "rich", "smart" ],
     *     "orders": [
     *       {
     *         "order_id": "xyz123412",
     *         "product_id": "SN9876543",
     *         "product": "Batman - The Movie (DVD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       },
     *       {
     *         "order_id": "xyz123411",
     *         "product_id": "SN9876542",
     *         "product": "Batman - The Soundtrack (CD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       }
     *     ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponse
     */
    public function upsertMultipleReceivers(string $groupId, array $receivers): CleverReachResponseInterface;

    /**
     * This is just like upsert, except you can increase or decrease numeric attribute values.
     * Prepend a "+" or "-" to the value to increase or decrease a value.
     *
     * You also can manipulate TAGS in a similar way:
     * In the array of tags, prepend a "-" to the tag you want to be removed.
     * To remove all tags with a specific origin, simply specify "*" instead of any tag name.
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0 and your receiver will definitely be inactive. 0 will eventually reactivate receivers!
     * Just omit 'deactivated' unless you know, what you are doing!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "attributes": {
     *       "weapons":"-4"
     *     },
     *     "global_attributes": {
     *       "title":"winner",
     *       "fights":"+1",
     *       "enemies":"-1"
     *     },
     *     "tags": [ "winner", "-loser", "-joker.*" ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponse
     */
    public function upsertplusMultipleReceivers(string $groupId, array $receivers): CleverReachResponseInterface;

    /**
     * Adds an event to a reveiver
     *
     * Example Post Data:
     * {
     *   "type": "log",
     *   "value": "Entered the bat cave",
     *   "type_id": "1939",
     *   "mailing_id": "28"
     * }
     *
     * @param string $groupId
     * @param array $receiverIdOrMail
     * @param EventStruct $event
     * @return CleverReachResponse
     */
    public function addReceiverEvent(string $groupId, array $receiverIdOrMail, EventStruct $event): CleverReachResponseInterface;

    /**
     * adds orderitem
     *
     * Example Post Data:
     * {
     *   "order_id": "19891990",
     *   "product": "Batman: The Video Game",
     *   "product_id": "jnr-1989-1990",
     *   "price": 1.99,
     *   "quantity": 2,
     *   "source": "fanshop-gotham",
     *   "currency": "USD"
     * }
     *
     * @param string $groupId
     * @param array $receiverIdOrMail
     * @param OrderStruct $order
     * @return CleverReachResponse
     */
    public function addOrder(string $groupId, array $receiverIdOrMail, OrderStruct $order): CleverReachResponseInterface;

    /**
     * update attribute
     *
     * @param string $groupId
     * @param string $attributeId
     * @param UpdateAttributeStruct $attribute
     * @return CleverReachResponse
     */
    public function updateAttribute(string $groupId, string $attributeId, UpdateAttributeStruct $attribute): CleverReachResponseInterface;

    /**
     * update attribute
     *
     *  Example Post Data:
     * {
     *   "name": "My Filter",
     *   "operator": "OR",
     *   "rules": [
     *     { "field":"email", "logic":"contains", "condition":"@gotham.com" },
     *     { "field":"activated", "logic":"bg", "condition":"1234567" },
     *     { "field":"firstname", "logic":"eq", "condition":"bruce" },
     *     { "field":"lastname", "logic":"eq", "condition":"wayne" }
     *   ]
     * }
     *
     * Possible Operators
     * OR
     * AND (default)
     *
     * Available Logics:
     * eq - equals
     * neq - not equals
     * sm - smaller
     * bg - bigger
     * isnull - is empty
     * notisnull - not empty
     * contains - contains given string
     * begins - begins with
     * ends - ends with
     * ismale - given field is male
     * isfemale - given field is female
     *
     * general available fields:
     * email - (email of the receiver)
     * source - (import source of the email)
     * imported - (timestamp of receiver import)
     * registered - (timestamp of receiver registration)
     * activated - (timestamp of receiver activation)
     * deactivated - (timestamp of receiver deactivation)
     * bounced - (timestamp of receiver bounce)
     * points - (points of the receiver)
     * last_location - (last location of receiver)
     * last_client - (last mail client used by receiver)
     * crevent_mail_send (logic: eq, condition: mailing_id -> receiver of certain mailing)
     * crevent_mail_open (logic: eq, condition: mailing_id -> opened mail of certain mailing)
     * crevent_mail_click (logic: eq, condition: mailing_id -> clicked on mail of certain mailing)
     * crevent_mail_bounce (logic: eq, condition: mailing_id -> bounced mail of certain mailing)
     * all attributes (if global and group attributes have the same name, group ones will be favoured)
     *
     * @param string $groupId
     * @param string $filterId
     * @param FilterStruct $filter
     * @return CleverReachResponse
     */
    public function updateFilter(string $groupId, string $filterId, FilterStruct $filter): CleverReachResponseInterface;

    /**
     * Updates receivers if email existing
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "registered": "1118354400",
     *     "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *     "deactivated": "0", // omit field unless you know what you are doing!
     *     "source":"Batcave Computer",
     *     "attributes": {
     *       "is_batman":"1"
     *     },
     *     "global_attributes": {
     *       "firstname":"Bruce",
     *       "lastname":"Wayne"
     *     },
     *     "tags": [ "hero", "rich", "smart" ],
     *     "orders": [
     *       {
     *         "order_id": "xyz123412",
     *         "product_id": "SN9876543",
     *         "product": "Batman - The Movie (DVD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       },
     *       {
     *         "order_id": "xyz123411",
     *         "product_id": "SN9876542",
     *         "product": "Batman - The Soundtrack (CD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       }
     *     ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponse
     */
    public function updateReceivers(string $groupId, array $receivers): CleverReachResponseInterface;

    /**
     * This is just like upsert, except you can increase or decrease numeric attribute values.
     * Prepend a "+" or "-" to the value to increase or decrease a value.
     *
     * You also can manipulate TAGS in a similar way:
     * In the array of tags, prepend a "-" to the tag you want to be removed.
     * To remove all tags with a specific origin, simply specify "*" instead of any tag name.
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0 and your receiver will definitely be inactive. 0 will eventually reactivate receivers!
     * Just omit 'deactivated' unless you know, what you are doing!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "attributes": {
     *       "weapons":"-4"
     *     },
     *     "global_attributes": {
     *       "title":"winner",
     *       "fights":"+1",
     *       "enemies":"-1"
     *     },
     *     "tags": [ "winner", "-loser", "-joker.*" ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponse
     */
    public function updatePlusReceivers(string $groupId, array $receivers): CleverReachResponseInterface;

    /**
     * Update any information of a receiver
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * {
     *   "email":"bruce@gotham.com",
     *   "registered": "1118354400",
     *   "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *   "deactivated": "0", // omit field unless you know what you are doing!
     *   "source":"Batcave Computer",
     *   "attributes": {
     *     "is_batman":"1"
     *   },
     *   "global_attributes": {
     *     "firstname":"Bruce",
     *     "lastname":"Wayne"
     *   },
     *   "tags": [ "hero", "rich", "smart" ],
     *   "orders": [
     *     {
     *       "order_id": "xyz123412",
     *       "product_id": "SN9876543",
     *       "product": "Batman - The Movie (DVD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     },
     *     {
     *       "order_id": "xyz123411",
     *       "product_id": "SN9876542",
     *       "product": "Batman - The Soundtrack (CD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     }
     *   ]
     * }
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @param ReceiverStruct $receiver
     * @return CleverReachResponse
     */
    public function updateReceiver(string $groupId, string $receiverIdOrMail, ReceiverStruct $receiver): CleverReachResponseInterface;

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponse
     */
    public function activateReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponse
     */
    public function deactivateReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * update value of an attribute of a receiver
     *
     * @param string $groupId
     * @param string $attributeId
     * @param string $receiverIdOrMail
     * @param UpdateAttributeStruct $attribute
     * @return CleverReachResponse
     */
    public function updateAttributeValue(string $groupId, string $attributeId, string $receiverIdOrMail, UpdateAttributeStruct $attribute): CleverReachResponseInterface;

    /**
     * updates orderitem
     *
     * Example Post Data:
     * {
     *   "order_id": "19891990",
     *   "product": "Batman: The Video Game",
     *   "product_id": "jnr-1989-1990",
     *   "price": 1.99,
     *   "quantity": 2,
     *   "source": "fanshop-gotham",
     *   "currency": "USD"
     * }
     *
     * @param string $groupId
     * @param string $orderId
     * @param string $receiverIdOrMail
     * @param OrderStruct $order
     * @return CleverReachResponse
     */
    public function updateOrder(string $groupId, string $orderId, string $receiverIdOrMail, OrderStruct $order): CleverReachResponseInterface;

    /**
     * update a list of receivers
     *
     * Example Post Data:
     * {
     *   "name": "Justice League",
     *   "receiver_info": "Superhero colleagues",
     *   "locked": false,
     *   "backup": true
     * }
     *
     * @param string $groupId
     * @param GroupStruct $group
     * @return CleverReachResponse
     */
    public function update(string $groupId, GroupStruct $group): CleverReachResponseInterface;

    /**
     * delete attribute
     *
     * @param string $groupId
     * @param string $attributeId
     * @return CleverReachResponse
     */
    public function deleteAttribute(string $groupId, string $attributeId): CleverReachResponseInterface;

    /**
     * delete attribute
     *
     * @param string $groupId
     * @param string $filterId
     * @return CleverReachResponse
     */
    public function deleteFilter(string $groupId, string $filterId): CleverReachResponseInterface;

    /**
     * delete receiver
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponse
     */
    public function deleteReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * delete order
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @param string $orderId
     * @return CleverReachResponse
     */
    public function deleteOrder(string $groupId, string $receiverIdOrMail, string $orderId): CleverReachResponseInterface;

    /**
     * delete group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponse
     */
    public function delete(string $groupId): CleverReachResponseInterface;

    /**
     * removes email address from group blacklist
     *
     * @param string $groupId
     * @param string $email
     * @return CleverReachResponse
     */
    public function removeBlacklistedMail(string $groupId, string $email): CleverReachResponseInterface;

    /**
     * delete all email addresses from group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponse
     */
    public function truncate(string $groupId): CleverReachResponseInterface;

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     */
    public function getTotalCountByGroupId(string $groupId): CleverReachCountResponseInterface;

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     */
    public function getActiveCountByGroupId(string $groupId): CleverReachCountResponseInterface;

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     */
    public function getInactiveCountByGroupId(string $groupId): CleverReachCountResponseInterface;

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     */
    public function getBounceCountByGroupId(string $groupId): CleverReachCountResponseInterface;
}
