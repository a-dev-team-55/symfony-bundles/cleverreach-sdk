<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:16
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

interface ClientPartialInterface
{

    /**
     * Returns a list of clients managed by this account. In the case of a single client it returns a object
     *
     * @param int $page         // Resultpage
     * @param int $pagesize     // max amount of entries per query.
     * @return CleverReachResponseInterface
     */
    public function getAll(int $page = 0, int $pagesize = 500): CleverReachResponseInterface;

    /**
     * get info for a single client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getOne(string $clientId): CleverReachResponseInterface;

    /**
     * get info for a single client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getActiveReceiverCountByClientId(string $clientId): CleverReachResponseInterface;

    /**
     * get the currently still available contignent of mails, free to send
     * For flatrate this always returns 0.
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getContingentByClientId(string $clientId): CleverReachResponseInterface;

    /**
     * get the current active invoice address for one client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getActiveInvoiceAddressByClientId(string $clientId): CleverReachResponseInterface;

    /**
     * get the next invoice date as a timestamp
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getNextInvoiceDateByClientId(string $clientId): CleverReachResponseInterface;

    /**
     * get subscription/plan info for one client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getSubscriptionPlanByClientId(string $clientId): CleverReachResponseInterface;

    /**
     * get the amount of receivers of a certain client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getReceiverCountByClientId(string $clientId): CleverReachResponseInterface;

    /**
     * get userlist for a single client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     */
    public function getUserlistByClientId(string $clientId): CleverReachResponseInterface;

    /**
     * get info for a single client by a given domain
     *
     * @param string $domain
     * @return CleverReachResponseInterface
     */
    public function getInfoByDomain(string $domain): CleverReachResponseInterface;
}
