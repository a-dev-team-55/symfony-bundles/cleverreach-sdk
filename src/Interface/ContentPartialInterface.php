<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:19
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Struct\ContentItemStruct;

interface ContentPartialInterface
{

    /**
     * gets a list of all "my content" items
     *
     * @return CleverReachResponseInterface
     */
    public function getAll(): CleverReachResponseInterface;

    /**
     * gets a certain "my content" item by id
     *
     * @param string $itemId
     * @return CleverReachResponseInterface
     */
    public function getOneByItemId(string $itemId): CleverReachResponseInterface;

    /**
     * adds a new "my content" item
     *
     * @param ContentItemStruct $item
     * @return CleverReachResponseInterface
     */
    public function add(ContentItemStruct $item): CleverReachResponseInterface;

    /**
     * modifies an existing "my content" item
     *
     * @param string $itemId
     * @param ContentItemStruct $item
     * @return CleverReachResponseInterface
     */
    public function updateByItemId(string $itemId, ContentItemStruct $item): CleverReachResponseInterface;

    /**
     * delete mycontent item
     *
     * @param string $itemId
     * @return CleverReachResponseInterface
     */
    public function delete(string $itemId): CleverReachResponseInterface;
}
