<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:20
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;

interface DebugPartialInterface
{

    /**
     * @return CleverReachResponseInterface
     */
    public function exchangeToken(): CleverReachResponseInterface;

    /**
     * @return CleverReachResponseInterface
     */
    public function getTokenTtl(): CleverReachResponseInterface;

    /**
     * @return CleverReachResponseInterface
     */
    public function validateToken(): CleverReachResponseInterface;

    /**
     * @return CleverReachResponseInterface
     */
    public function getCurrentClientInfo(): CleverReachResponseInterface;
}
