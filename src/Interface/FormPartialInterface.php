<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:22
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;

interface FormPartialInterface
{

    /**
     * @return CleverReachResponseInterface
     */
    public function getAll(): CleverReachResponseInterface;

    /**
     * @param string $formId
     * @return CleverReachResponseInterface
     */
    public function getOneByFormId(string $formId): CleverReachResponseInterface;

    /**
     * return code for embedding
     *
     * Note:
     * This Explorer unescapes the return values.
     * Therefore, it unescapes HTML from this endpoint!
     * Your browser tries to display it, resulting in no result and seemingly endless loading.
     * Please use this endpoint in your browser directly to try it.
     *
     * @param string $formId
     * @param bool $badget      // Enable Badget (Disable only for non free plans) (default: false).
     * @param bool $embedded    // Embedded (default: false).
     * @param bool $js          // Embedded JS (default: true).
     * @param bool $css         // Embedded CSS (default: true).
     * @return CleverReachResponseInterface
     */
    public function getEmbeddingCode(
        string $formId,
        bool $badget = false,
        bool $embedded = false,
        bool $js = true,
        bool $css = true
    ): CleverReachResponseInterface;

    /**
     * @param string $formId
     * @param string $email
     * @param array|null $doidata
     * @return CleverReachResponseInterface
     */
    public function sendSubscribeMail(string $formId, string $email, ?array $doidata = null): CleverReachResponseInterface;

    /**
     * @param string $formId
     * @param string $email
     * @param array|null $doidata
     * @return CleverReachResponseInterface
     */
    public function sendUnsubscribeMail(string $formId, string $email, ?array $doidata = null): CleverReachResponseInterface;

    /**
     * Creates a form from a template by given type
     *
     * Example Post Data (extradata for type 'default'):
     * {
     *   "name": "Form for Batnews",
     *   "title": "Batnews"
     * }
     *
     * You should specify a name and a title.
     * If no name is given, the title is taken.
     * 400 if you don't specify anything.
     *
     * @param string $groupId
     * @param string $type
     * @param array $extradata
     * @return CleverReachResponseInterface
     */
    public function createFromTemplate(string $groupId, string $type = 'default', array $extradata = []): CleverReachResponseInterface;

    /**
     * @param string $formId
     * @return CleverReachResponseInterface
     */
    public function delete(string $formId): CleverReachResponseInterface;
}
