<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:34
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Constant\HookEvent;

interface HookPartialInterface
{

    /**
     * @param string $url
     * @param string $groupId
     * @param HookEvent $event
     * @return CleverReachResponseInterface
     */
    public function register(string $url, string $groupId, HookEvent $event): CleverReachResponseInterface;

    /**
     * @return CleverReachResponseInterface
     */
    public function getAll(): CleverReachResponseInterface;

    /**
     * @param string $groupId
     * @param HookEvent $event
     * @return CleverReachResponseInterface
     */
    public function delete(string $groupId, HookEvent $event): CleverReachResponseInterface;
}
