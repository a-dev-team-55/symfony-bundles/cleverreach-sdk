<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:37
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

interface OAuthPartialInterface
{

    /**
     * delete mycontent item
     *
     * @return CleverReachResponseInterface
     */
    public function delete(): CleverReachResponseInterface;
}
