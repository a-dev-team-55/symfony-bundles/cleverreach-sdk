<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:15
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

interface BouncePartialInterface
{

    /**
     * returns a list with all bounces for your account
     *
     * Categories (exerpt):
     * - permanent/hardbounce: mailbox does not exist
     * - softbounce: temporary errors
     * - blocked: receiving mailserver didn't accept our email
     * - delayed: temporary delivery error
     * - spam: receiving mailserver categorized your email as spam
     *
     * There are also two types of bounces:
     * - type: mailing -- bounce was created through a mailing
     * - type: doi -- bounce was created while sending the DOI emails (form signup)
     * type_id is either the mailing id or the form id depending on the type
     *
     * @param int $page         // Resultpage
     * @param int $pagesize     // max amount of entries per query.
     * @return CleverReachResponseInterface
     */
    public function getAll(int $page = 0, int $pagesize = 500): CleverReachResponseInterface;
}
