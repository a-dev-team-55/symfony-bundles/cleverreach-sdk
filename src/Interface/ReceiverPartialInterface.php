<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:39
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Constant\ReceiverState;
use ADevTeam\CleverReachBundle\Struct\CloneReceiverStruct;
use ADevTeam\CleverReachBundle\Struct\DeleteReceiversStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverEventStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverFilterStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverOrderStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverValidationStruct;

interface ReceiverPartialInterface
{

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     */
    public function getOne(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     */
    public function getAttributesByReceiverId(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * @param string $receiverIdOrMail
     * @param ReceiverState $state
     * @return CleverReachResponseInterface
     */
    public function getReceiverGroupsByReceiverId(string $receiverIdOrMail, ReceiverState $state = ReceiverState::ALL): CleverReachResponseInterface;

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     */
    public function getEventsByGroupId(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface;

    /**
     * @return CleverReachResponseInterface
     */
    public function getBounced(): CleverReachResponseInterface;

    /**
     * @param string $receiverIdOrMail
     * @param CloneReceiverStruct $newReceiver
     * @return CleverReachResponseInterface
     */
    public function clone(string $receiverIdOrMail, CloneReceiverStruct $newReceiver): CleverReachResponseInterface;

    /**
     * @param string $receiverIdOrMail
     * @param ReceiverEventStruct $event
     * @return CleverReachResponseInterface
     */
    public function addEvent(string $receiverIdOrMail, ReceiverEventStruct $event): CleverReachResponseInterface;

    /**
     * @param string $receiverIdOrMail
     * @param ReceiverOrderStruct $order
     * @return CleverReachResponseInterface
     */
    public function addOrder(string $receiverIdOrMail, ReceiverOrderStruct $order): CleverReachResponseInterface;

    /**
     * Delete receivers by given list of ids of emails
     *
     * Example Post Data:
     * {
     *   "receivers": [
     *     "381940",
     *     "dick.grayson@wayne.com",
     *     "robin@gotham.com",
     *     "3571983"
     *   ],
     *   "group_id": 271939
     * }
     *
     * @param DeleteReceiversStruct $deletableReceivers
     * @return CleverReachResponseInterface
     */
    public function deleteMultiple(DeleteReceiversStruct $deletableReceivers): CleverReachResponseInterface;

    /**
     * returns a list of receivers, filtered by a temporary filter
     *
     * Example Post Data:
     * {
     *   "groups": ["1","2","3"],
     *   "operator": "OR",
     *   "rules": [
     *     { "field":"email", "logic":"contains", "condition":"@gotham.com" },
     *     { "field":"activated", "logic":"bg", "condition":"1234567" },
     *     { "field":"firstname", "logic":"eq", "condition":"bruce" },
     *     { "field":"lastname", "logic":"eq", "condition":"gordon" }
     *   ],
     *   "orderby": "activated desc",
     *   "detail": 0,
     *   "pagesize": 250,
     *   "page": 0
     * }
     *
     * General available fields:
     * email - (email of the receiver)
     * source - (import source of the email)
     * imported - (timestamp of receiver import)
     * registered - (timestamp of receiver registration)
     * activated - (timestamp of receiver activation)
     * deactivated - (timestamp of receiver deactivation)
     * bounced - (timestamp of receiver bounce)
     * points - (points of the receiver)
     * last_location - (last location of receiver)
     * last_client - (last mail client used by receiver)
     * crevent_mail_send (logic: eq, condition: mailing_id -> receiver of certain mailing)
     * crevent_mail_open (logic: eq, condition: mailing_id -> opened mail of certain mailing)
     * crevent_mail_click (logic: eq, condition: mailing_id -> clicked on mail of certain mailing)
     * crevent_mail_bounce (logic: eq, condition: mailing_id -> bounced mail of certain mailing)
     * all attributes (if global and group attributes have the same name, group ones will be favoured)
     *
     * @param ReceiverFilterStruct $filter
     * @return CleverReachResponseInterface
     */
    public function getByTemporaryFilter(ReceiverFilterStruct $filter): CleverReachResponseInterface;

    /**
     * Submit a list of emails and return the valid ones.
     * Returns the not blacklisted emails of given list.
     * If group_id is set, the active state is checked additionally.
     *
     * Example Post Data:
     * {
     *   "emails": [
     *     "joker@arkham.com",
     *     "twoface@dent.com",
     *     "batman@gotham.com"
     *   ],
     *   "group_id": "1939",
     *   "invert": false
     * }
     *
     * @param ReceiverValidationStruct $receivers
     * @return CleverReachResponseInterface
     */
    public function validate(ReceiverValidationStruct $receivers): CleverReachResponseInterface;

    /**
     * Changes the email of a certain receiver
     *
     * @param string $receiverIdOrMail
     * @param string $newEmail
     * @param bool $overwrite
     * @return CleverReachResponseInterface
     */
    public function updateEmailByReceiverId(string $receiverIdOrMail, string $newEmail, bool $overwrite = false): CleverReachResponseInterface;

    /**
     * update value of an attribute of a receiver
     *
     * @param string $receiverIdOrMail
     * @param string $attributeId
     * @param mixed $value
     * @return CleverReachResponseInterface
     */
    public function updateAttributeByReceiverId(string $receiverIdOrMail, string $attributeId, mixed $value): CleverReachResponseInterface;

    /**
     * updates orderitem
     *
     * @param string $receiverIdOrMail
     * @param string $orderId
     * @param ReceiverOrderStruct $order
     * @return CleverReachResponseInterface
     */
    public function updateOrderByReceiverId(string $receiverIdOrMail, string $orderId, ReceiverOrderStruct $order): CleverReachResponseInterface;

    /**
     * updates orderitem
     *
     * @param string $receiverIdOrMail
     * @param string|null $groupId
     * @return CleverReachResponseInterface
     */
    public function delete(string $receiverIdOrMail, ?string $groupId = null): CleverReachResponseInterface;

    /**
     * updates orderitem
     *
     * @param string $receiverIdOrMail
     * @param string $orderId
     * @return CleverReachResponseInterface
     */
    public function deleteOrder(string $receiverIdOrMail, string $orderId): CleverReachResponseInterface;
}
