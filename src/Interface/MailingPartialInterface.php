<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:35
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

use ADevTeam\CleverReachBundle\Constant\MailingState;
use ADevTeam\CleverReachBundle\Struct\MailingStruct;
use DateTimeInterface;

interface MailingPartialInterface
{

    /**
     * returns lists of mailings according to following filter settings
     *
     * NOTE: The start and end fields depend on the state field.
     * - finished mails are filtered by finished time
     * - draft mails are filtered by creation time
     * - waiting mails are filtered by the time they will be sent
     * - running mails are filtered by the time they have been started
     *
     * @param int $limit                        // 	number of items.
     * @param MailingState $state               // state of mailing.
     * @param string|null $channelId            // (optional): if specified, it will only return matching results.
     * @param DateTimeInterface|null $start     // (optional) Filter by start time (see note).
     * @param DateTimeInterface|null $end       // (optional) Filter by end time (see note).
     * @return CleverReachResponseInterface
     */
    public function getAll(
        int $limit = 25,
        MailingState $state = MailingState::ALL,
        ?string $channelId = null,
        ?DateTimeInterface $start = null,
        ?DateTimeInterface $end = null
    ): CleverReachResponseInterface;

    /**
     * get a mailing by id
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     */
    public function getOneByMailingId(string $mailingId): CleverReachResponseInterface;

    /**
     * get links for a mailing by id
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     */
    public function getLinksByMailingId(string $mailingId): CleverReachResponseInterface;

    /**
     * return a list of orders
     *
     * @param string $reportIdOrMailingId
     * @return CleverReachResponseInterface
     */
    public function getOrdersByMailingId(string $reportIdOrMailingId): CleverReachResponseInterface;

    /**
     * get random receiver for a mailing for preview reasons
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     */
    public function getRandomReceiverByMailingId(string $mailingId): CleverReachResponseInterface;

    /**
     * returns array of categories
     *
     * @return CleverReachResponseInterface
     */
    public function getCategories(): CleverReachResponseInterface;

    /**
     * specific category
     *
     * @param string $categoryId
     * @return CleverReachResponseInterface
     */
    public function getCategoryByCategoryId(string $categoryId): CleverReachResponseInterface;

    /**
     * returns agency templates, if there are any
     *
     * @return CleverReachResponseInterface
     */
    public function getAgencyTemplates(): CleverReachResponseInterface;

    /**
     * returns user templates, if there are any
     *
     * @return CleverReachResponseInterface
     */
    public function getUserTemplates(): CleverReachResponseInterface;

    /**
     * create a new mailing
     *
     * Example Post Data:
     * {
     *   "name": "my internal name",
     *   "subject": "subject line",
     *   "sender_name": "Bruce Wayne (Wayne corp.)",
     *   "sender_email": "bruce.wayne@gotham.com",
     *   "content": {
     *     "type": "html/text",                     // "html", "text" or "html/text"
     *     "html": "<html><body>The two faces of Harvey Dent</body></html>",
     *     "text": "The two faces of Harvey Dent"
     *   },
     *   "receivers": {                             // fill either a list of group ids or a filter id
     *     "groups": [ "31939", "81942" ],
     *     "filter": "66"
     *   },
     *   "settings": {
     *     "editor": "wizard",                      // "wizard", "freeform", "advanced", "plaintext"
     *     "open_tracking": true,                   // track opening of emails
     *     "click_tracking": true,                  // track clicks of emails
     *     "link_tracking_url": "27.wayne.cleverreach.com",
     *     "link_tracking_type": "google",          // "google", "intelliad", "crconnect"
     *     "google_campaign_name": "My Campaign",   // in case of link_tracking_type == "google"
     *     "unsubscribe_form_id": "23",
     *     "category_id": "54"
     *   },
     *   "tags": ["setup_v2"]                       // use "setup_v2" for new mailing editor
     * }
     *
     * Note: For links not to be substituted by tracking links, use 'single quotes' around urls in your html.
     *
     * @param MailingStruct $mailing
     * @return CleverReachResponseInterface
     */
    public function add(MailingStruct $mailing): CleverReachResponseInterface;

    /**
     * @param string $mailingId
     * @param string[] $receiverMailAddresses
     * @param string $previewText
     * @return CleverReachResponseInterface
     */
    public function sendPreview(string $mailingId, array $receiverMailAddresses, string $previewText = ''): CleverReachResponseInterface;

    /**
     * @param string $internalName
     * @param string $html
     * @param string[] $tags
     * @return CleverReachResponseInterface
     */
    public function createTemplate(string $internalName, string $html, array $tags = []): CleverReachResponseInterface;

    /**
     * update mailing
     *
     * Example Post Data:
     * {
     *   "name": "my internal name",
     *   "subject": "subject line",
     *   "sender_name": "Bruce Wayne (Wayne corp.)",
     *   "sender_email": "bruce.wayne@gotham.com",
     *   "content": {
     *     "type": "html/text",                     // "html", "text" or "html/text"
     *     "html": "<html><body>The two faces of Harvey Dent</body></html>",
     *     "text": "The two faces of Harvey Dent"
     *   },
     *   "receivers": {                             // fill either a list of group ids or a filter id
     *     "groups": [ "31939", "81942" ],
     *     "filter": "66"
     *   },
     *   "settings": {
     *     "editor": "wizard",                      // "wizard", "freeform", "advanced", "plaintext"
     *     "open_tracking": true,                   // track opening of emails
     *     "click_tracking": true,                  // track clicks of emails
     *     "link_tracking_url": "27.wayne.cleverreach.com",
     *     "link_tracking_type": "google",          // "google", "intelliad", "crconnect"
     *     "google_campaign_name": "My Campaign",   // in case of link_tracking_type == "google"
     *     "unsubscribe_form_id": "23",
     *     "category_id": "54"
     *   },
     *   "tags": ["setup_v2"]                       // use "setup_v2" for new mailing editor
     * }
     *
     * @param string $mailingId
     * @param MailingStruct $mailing
     * @return CleverReachResponseInterface
     */
    public function update(string $mailingId, MailingStruct $mailing): CleverReachResponseInterface;

    /**
     * Delete a certain report
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     */
    public function delete(string $mailingId): CleverReachResponseInterface;
}
