<?php
/*
 * @author ADevTeam
 * @created 27.11.2023 21:44
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Interface;

interface PartialInterface
{

    /**
     * @return string
     */
    public function __toString(): string;
}
