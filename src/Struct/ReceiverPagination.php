<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 27.11.2023 00:08
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\FilterDetailDepth;
use ADevTeam\CleverReachBundle\Constant\OrderDirection;
use ADevTeam\CleverReachBundle\Constant\ReceiverType;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class ReceiverPagination
 * @package ADevTeam\CleverReachBundle\Struct
 */
class ReceiverPagination implements StructInterface
{

    /**
     * @var int
     */
    private int $pageSize = 50;

    /**
     * @var int
     */
    private int $page = 0;

    /**
     * @var ReceiverType
     */
    private ReceiverType $type = ReceiverType::ALL;

    /**
     * @var FilterDetailDepth
     */
    private FilterDetailDepth $detailDepth = FilterDetailDepth::NONE;

    /**
     * @var array
     */
    private array $emailList = [];

    /**
     * @var OrderDirection
     */
    private OrderDirection $orderDirection = OrderDirection::DESC;

    /**
     * @var array
     */
    private array $receiverIdList = [];

    /**
     * @var string|null
     */
    private ?string $orderBy = null;

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     * @return $this
     */
    public function setPageSize(int $pageSize): ReceiverPagination
    {
        $this->pageSize = $pageSize;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): ReceiverPagination
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return ReceiverType
     */
    public function getType(): ReceiverType
    {
        return $this->type;
    }

    /**
     * @param ReceiverType $type
     * @return $this
     */
    public function setType(ReceiverType $type): ReceiverPagination
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return FilterDetailDepth
     */
    public function getDetailDepth(): FilterDetailDepth
    {
        return $this->detailDepth;
    }

    /**
     * @param FilterDetailDepth $detailDepth
     * @return $this
     */
    public function setDetailDepth(FilterDetailDepth $detailDepth): ReceiverPagination
    {
        $this->detailDepth = $detailDepth;
        return $this;
    }

    /**
     * @return array
     */
    public function getEmailList(): array
    {
        return $this->emailList;
    }

    /**
     * @param array $emailList
     * @return $this
     */
    public function setEmailList(array $emailList): ReceiverPagination
    {
        $this->emailList = $emailList;
        return $this;
    }

    /**
     * @param string $receiverEmail
     * @return $this
     */
    public function addReceiverEmail(string $receiverEmail): ReceiverPagination
    {
        $this->emailList[] = $receiverEmail;
        return $this;
    }

    /**
     * @return OrderDirection
     */
    public function getOrderDirection(): OrderDirection
    {
        return $this->orderDirection;
    }

    /**
     * @param OrderDirection $orderDirection
     * @return $this
     */
    public function setOrderDirection(OrderDirection $orderDirection): ReceiverPagination
    {
        $this->orderDirection = $orderDirection;
        return $this;
    }

    /**
     * @return array
     */
    public function getReceiverIdList(): array
    {
        return $this->receiverIdList;
    }

    /**
     * @param array $receiverIdList
     * @return $this
     */
    public function setReceiverIdList(array $receiverIdList): ReceiverPagination
    {
        $this->receiverIdList = $receiverIdList;
        return $this;
    }

    /**
     * @param string|int $receiverId
     * @return $this
     */
    public function addReceiverId(string|int $receiverId): ReceiverPagination
    {
        $this->receiverIdList[] = $receiverId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    /**
     * @param string|null $orderBy
     * @return $this
     */
    public function setOrderBy(?string $orderBy): ReceiverPagination
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'pagesize' => $this->pageSize,
            'page' => $this->page,
            'type' => $this->type->value,
            'detail' => $this->detailDepth->value
        ];

        if (!empty($this->emailList)) {
            $data['email_list'] = implode(',', $this->emailList);
        }

        if (!empty($this->receiverIdList)) {
            $data['id_list'] = implode(',', $this->receiverIdList);
        }

        if ($this->orderBy) {
            $data['orderby'] = sprintf('%s %s', $this->orderBy, $this->orderDirection->value);
        }

        return $data;
    }
}
