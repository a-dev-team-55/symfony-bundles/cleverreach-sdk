<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:58
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class MailingReceiverStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class MailingReceiverStruct implements StructInterface
{

    /**
     * @var array
     */
    private array $groupIds = [];

    /**
     * @var int|null
     */
    private ?int $filterId = null;

    /**
     * @return array
     */
    public function getGroupIds(): array
    {
        return $this->groupIds;
    }

    /**
     * @param array $groupIds
     * @return $this
     */
    public function setGroupIds(array $groupIds): MailingReceiverStruct
    {
        $this->groupIds = $groupIds;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFilterId(): ?int
    {
        return $this->filterId;
    }

    /**
     * @param int|null $filterId
     * @return $this
     */
    public function setFilterId(?int $filterId): MailingReceiverStruct
    {
        $this->filterId = $filterId;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if (!empty($this->groupIds)) {
            $data['groups'] = $this->groupIds;
        }

        if ($this->filterId) {
            $data['filter'] = $this->filterId;
        }

        return $data;
    }
}
