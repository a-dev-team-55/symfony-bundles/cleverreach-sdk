<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 23:24
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class BlacklistedEmail
 * @package ADevTeam\CleverReachBundle\Struct
 */
class BlacklistedEmail implements StructInterface
{

    /**
     * @var string
     */
    private string $email;

    /**
     * @var string|null
     */
    private ?string $comment = null;

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): BlacklistedEmail
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return $this
     */
    public function setComment(?string $comment): BlacklistedEmail
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        $data = [
            'email' => $this->email
        ];

        if ($this->comment) {
            $data['comment'] = $this->comment;
        }

        return $data;
    }

}
