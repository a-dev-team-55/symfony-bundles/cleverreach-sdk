<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:06
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

/**
 * @class FilterOperator
 * @package ADevTeam\CleverReachBundle\Struct
 */
enum FilterOperatorStruct: string
{
    case OR = 'OR';
    case AND = 'AND';
}
