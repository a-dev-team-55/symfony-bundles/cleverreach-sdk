<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 20:02
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\MailingContentType;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class MailingContentStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class MailingContentStruct implements StructInterface
{

    /**
     * @var MailingContentType|null
     */
    private ?MailingContentType $type = null;

    /**
     * @var string|null
     */
    private ?string $html = null;

    /**
     * @var string|null
     */
    private ?string $text = null;

    /**
     * @return MailingContentType|null
     */
    public function getType(): ?MailingContentType
    {
        return $this->type;
    }

    /**
     * @param MailingContentType|null $type
     * @return $this
     */
    public function setType(?MailingContentType $type): MailingContentStruct
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHtml(): ?string
    {
        return $this->html;
    }

    /**
     * @param string|null $html
     * @return $this
     */
    public function setHtml(?string $html): MailingContentStruct
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return $this
     */
    public function setText(?string $text): MailingContentStruct
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if ($this->type) {
            $data['type'] = $this->type->value;
        }

        if ($this->html) {
            $data['html'] = $this->html;
        }

        if ($this->text) {
            $data['text'] = $this->text;
        }

        return $data;
    }
}
