<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:31
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Exception\InvalidEmailException;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class ReceiverValidationStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class ReceiverValidationStruct implements StructInterface
{

    /**
     * @var string|null
     */
    private ?string $groupId = null;

    /**
     * @var string[]
     */
    private array $emails = [];

    /**
     * @var bool
     */
    private bool $invert = false;

    /**
     * @return string|null
     */
    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    /**
     * @param string|null $groupId
     * @return $this
     */
    public function setGroupId(?string $groupId): ReceiverValidationStruct
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getEmails(): array
    {
        return $this->emails;
    }

    /**
     * @param array $emails
     * @return $this
     * @throws InvalidEmailException
     */
    public function setEmails(array $emails): ReceiverValidationStruct
    {
        foreach ($emails as $email) {
            $this->addEmail($email);
        }

        return $this;
    }

    /**
     * @param string $email
     * @return $this
     * @throws InvalidEmailException
     */
    public function addEmail(string $email): ReceiverValidationStruct
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException($email);
        }

        $this->emails[] = $email;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInvert(): bool
    {
        return $this->invert;
    }

    /**
     * @param bool $invert
     * @return $this
     */
    public function setInvert(bool $invert): ReceiverValidationStruct
    {
        $this->invert = $invert;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'emails' => $this->emails,
            'invert' => $this->invert
        ];

        if ($this->groupId) {
            $data['group_id'] = $this->groupId;
        }

        return $data;
    }
}
