<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 22.11.2023 09:35
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * @class EventStruct
 * @package ADevTeam\CleverReachBundle\Interface
 */
class EventStruct implements StructInterface
{

    /**
     * @var string
     */
    protected string $value;

    /**
     * @var string
     */
    protected string $type = 'log';

    /**
     * @var string|null
     */
    protected ?string $typeId = null;

    /**
     * @var string|null
     */
    protected ?string $mailingId = null;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTypeId(): ?string
    {
        return $this->typeId;
    }

    /**
     * @param string|null $typeId
     * @return $this
     */
    public function setTypeId(?string $typeId): self
    {
        $this->typeId = $typeId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMailingId(): ?string
    {
        return $this->mailingId;
    }

    /**
     * @param string|null $mailingId
     * @return $this
     */
    public function setMailingId(?string $mailingId): self
    {
        $this->mailingId = $mailingId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        $data = [
            'value' => $this->value
        ];

        if ($this->type) {
            $data['type'] = $this->type;
        }

        if ($this->typeId) {
            $data['type_id'] = $this->typeId;
        }

        if ($this->mailingId) {
            $data['mailing_id'] = $this->mailingId;
        }

        return $data;
    }
}
