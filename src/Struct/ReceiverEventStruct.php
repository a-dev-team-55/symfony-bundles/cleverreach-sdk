<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 22.11.2023 09:35
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * @class ReceiverEventStruct
 * @package ADevTeam\CleverReachBundle\Interface
 */
class ReceiverEventStruct extends EventStruct implements StructInterface
{

    /**
     * @var string|null
     */
    private ?string $groupId = null;


    /**
     * @return string|null
     */
    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    /**
     * @param string|null $groupId
     * @return $this
     */
    public function setGroupId(?string $groupId): ReceiverEventStruct
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        $data = parent::toArray();

        if ($this->groupId) {
            $data['group_id'] = $this->groupId;
        }

        return $data;
    }
}
