<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 27.11.2023 00:39
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class Filter
 * @package ADevTeam\CleverReachBundle\Struct
 */
class FilterStruct implements StructInterface
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var FilterOperatorStruct
     */
    private FilterOperatorStruct $operator = FilterOperatorStruct::OR;

    /**
     * @var FilterRuleStruct[]
     */
    private array $rules = [];

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): FilterStruct
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return FilterOperatorStruct
     */
    public function getOperator(): FilterOperatorStruct
    {
        return $this->operator;
    }

    /**
     * @param FilterOperatorStruct $operator
     * @return $this
     */
    public function setOperator(FilterOperatorStruct $operator): FilterStruct
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return FilterRuleStruct[]
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     * @return $this
     */
    public function setRules(array $rules): FilterStruct
    {
        $this->rules = $rules;
        return $this;
    }

    /**
     * @param FilterRuleStruct $rule
     * @return $this
     */
    public function addRule(FilterRuleStruct $rule): FilterStruct
    {
        $this->rules[] = $rule;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'operator' => $this->operator->value,
            'rules' => array_map(fn(FilterRuleStruct $r) => $r->toArray(), $this->rules),
        ];
    }
}
