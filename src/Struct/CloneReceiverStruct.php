<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 21:38
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class CloneReceiverStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class CloneReceiverStruct implements StructInterface
{

    /**
     * @var string
     */
    private string $email;

    /**
     * @var string
     */
    private string $targetGroupId;

    /**
     * @var bool
     */
    private bool $override = false;

    /**
     * @param string $email
     * @param string $targetGroupId
     */
    public function __construct(string $email, string $targetGroupId)
    {
        $this->email = $email;
        $this->targetGroupId = $targetGroupId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): CloneReceiverStruct
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getTargetGroupId(): string
    {
        return $this->targetGroupId;
    }

    /**
     * @param string $targetGroupId
     * @return $this
     */
    public function setTargetGroupId(string $targetGroupId): CloneReceiverStruct
    {
        $this->targetGroupId = $targetGroupId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOverride(): bool
    {
        return $this->override;
    }

    /**
     * @param bool $override
     * @return $this
     */
    public function setOverride(bool $override): CloneReceiverStruct
    {
        $this->override = $override;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'email' => $this->email,
            'target_group_id' => $this->targetGroupId
        ];

        if ($this->override) {
            $data['override'] = $this->override;
        }

        return $data;
    }
}
