<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 21:58
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class DeleteReceiversStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class DeleteReceiversStruct extends ReceiverList implements StructInterface
{

    /**
     * @var string
     */
    private string $groupId;

    /**
     * @param string $groupId
     */
    public function __construct(string $groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     * @return $this
     */
    public function setGroupId(string $groupId): DeleteReceiversStruct
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();

        $data['group_id'] = $this->groupId;

        return $data;
    }
}
