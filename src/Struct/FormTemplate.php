<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 23:15
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class FormTemplate
 * @package ADevTeam\CleverReachBundle\Struct
 */
class FormTemplate implements StructInterface
{

    /**
     * @var string
     */
    private string $groupId;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var array
     */
    private array $extradata = [];

    /**
     * @param string $groupId
     * @param string $type
     * @param array $extradata
     */
    public function __construct(string $groupId, string $type, array $extradata)
    {
        $this->groupId = $groupId;
        $this->type = $type;
        $this->extradata = $extradata;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     * @return $this
     */
    public function setGroupId(string $groupId): FormTemplate
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): FormTemplate
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array
     */
    public function getExtradata(): array
    {
        return $this->extradata;
    }

    /**
     * @param array $extradata
     * @return $this
     */
    public function setExtradata(array $extradata): FormTemplate
    {
        $this->extradata = $extradata;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'groupid' => $this->groupId,
            'type' => $this->type,
            'extradata' => json_encode($this->extradata)
        ];
    }

}
