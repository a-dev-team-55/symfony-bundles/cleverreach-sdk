<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 17:49
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use DateTime;
use DateTimeInterface;
use Exception;

/**
 * @class ReceiverStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class ReceiverStruct
{


    /**
     * @var string
     */
    private string $email;

    /**
     * @var DateTimeInterface|int|null
     */
    private int|null|DateTimeInterface $imported = null;

    /**
     * @var DateTimeInterface|int|null
     */
    private int|null|DateTimeInterface $registered = null;

    /**
     * @var DateTimeInterface|int|null
     */
    private int|null|DateTimeInterface $activated = null;

    /**
     * @var DateTimeInterface|int|null
     */
    private int|null|DateTimeInterface $deactivated = null;

    /**
     * @var DateTimeInterface|int|null
     */
    private int|null|DateTimeInterface $bounced = null;

    /**
     * @var string|null
     */
    private ?string $source = null;

    /**
     * @var string|null
     */
    private ?string $lastIp = null;

    /**
     * @var string|null
     */
    private ?string $lastLocation = null;

    /**
     * @var string|null
     */
    private ?string $lastClient = null;

    /**
     * @var int|null
     */
    private ?int $points = null;

    /**
     * @var int|null
     */
    private ?int $stars = null;

    /**
     * @var int|null
     */
    private ?int $id = null;

    /**
     * @var int|null
     */
    private ?int $groupId = null;

    /**
     * @var int
     */
    private int $conversionRate = 0;

    /**
     * @var int
     */
    private int $openRate = 0;

    /**
     * @var int
     */
    private int $clickRate = 0;

    /**
     * @var bool
     */
    private bool $active = false;

    /**
     * @var array
     */
    private array $attributes = [];

    /**
     * @var array
     */
    private array $globalAttributes = [];

    /**
     * @var array
     */
    private array $tags = [];

    /**
     * @var OrderStruct[] $orders
     */
    private array $orders = [];

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): ReceiverStruct
    {
        $this->email = $email;
        return $this;
    }

    public function getImported(): DateTimeInterface|int|null
    {
        return $this->imported;
    }

    public function setImported(DateTimeInterface|int|null $imported): ReceiverStruct
    {
        $this->imported = $imported;
        return $this;
    }

    public function getRegistered(): DateTimeInterface|int|null
    {
        return $this->registered;
    }

    public function setRegistered(DateTimeInterface|int|null $registered): ReceiverStruct
    {
        $this->registered = $registered;
        return $this;
    }

    public function getActivated(): DateTimeInterface|int|null
    {
        return $this->activated;
    }

    public function setActivated(DateTimeInterface|int|null $activated): ReceiverStruct
    {
        $this->activated = $activated;
        return $this;
    }

    public function getDeactivated(): DateTimeInterface|int|null
    {
        return $this->deactivated;
    }

    public function setDeactivated(DateTimeInterface|int|null $deactivated): ReceiverStruct
    {
        $this->deactivated = $deactivated;
        return $this;
    }

    public function getBounced(): DateTimeInterface|int|null
    {
        return $this->bounced;
    }

    public function setBounced(DateTimeInterface|int|null $bounced): ReceiverStruct
    {
        $this->bounced = $bounced;
        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): ReceiverStruct
    {
        $this->source = $source;
        return $this;
    }

    public function getLastIp(): ?string
    {
        return $this->lastIp;
    }

    public function setLastIp(?string $lastIp): ReceiverStruct
    {
        $this->lastIp = $lastIp;
        return $this;
    }

    public function getLastLocation(): ?string
    {
        return $this->lastLocation;
    }

    public function setLastLocation(?string $lastLocation): ReceiverStruct
    {
        $this->lastLocation = $lastLocation;
        return $this;
    }

    public function getLastClient(): ?string
    {
        return $this->lastClient;
    }

    public function setLastClient(?string $lastClient): ReceiverStruct
    {
        $this->lastClient = $lastClient;
        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(?int $points): ReceiverStruct
    {
        $this->points = $points;
        return $this;
    }

    public function getStars(): ?int
    {
        return $this->stars;
    }

    public function setStars(?int $stars): ReceiverStruct
    {
        $this->stars = $stars;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ReceiverStruct
    {
        $this->id = $id;
        return $this;
    }

    public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    public function setGroupId(?int $groupId): ReceiverStruct
    {
        $this->groupId = $groupId;
        return $this;
    }

    public function getConversionRate(): int
    {
        return $this->conversionRate;
    }

    public function setConversionRate(int $conversionRate): ReceiverStruct
    {
        $this->conversionRate = $conversionRate;
        return $this;
    }

    public function getOpenRate(): int
    {
        return $this->openRate;
    }

    public function setOpenRate(int $openRate): ReceiverStruct
    {
        $this->openRate = $openRate;
        return $this;
    }

    public function getClickRate(): int
    {
        return $this->clickRate;
    }

    public function setClickRate(int $clickRate): ReceiverStruct
    {
        $this->clickRate = $clickRate;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): ReceiverStruct
    {
        $this->active = $active;
        return $this;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes): ReceiverStruct
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function getGlobalAttributes(): array
    {
        return $this->globalAttributes;
    }

    public function setGlobalAttributes(array $globalAttributes): ReceiverStruct
    {
        $this->globalAttributes = $globalAttributes;
        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): ReceiverStruct
    {
        $this->tags = $tags;
        return $this;
    }

    public function getOrders(): array
    {
        return $this->orders;
    }

    public function setOrders(array $orders): ReceiverStruct
    {
        $this->orders = $orders;
        return $this;
    }

    /**
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int $hour
     * @param int $minutes
     * @param int $seconds
     * @return ReceiverStruct
     */
    public function setImportedByReadableDate(
        int $day,
        int $month,
        int $year,
        int $hour = 12,
        int $minutes = 0,
        int $seconds = 0,
    ): ReceiverStruct
    {
        try {
            $date = new DateTime(sprintf('%s.%s.%s %s:%s:%s', $day, $month, $year, $hour, $minutes, $seconds));

            $this->imported = $date;
        } catch (Exception $e) {
            $this->imported = new DateTime();
        }

        return $this;
    }
    /**
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int $hour
     * @param int $minutes
     * @param int $seconds
     * @return ReceiverStruct
     */
    public function setRegisteredByReadableDate(
        int $day,
        int $month,
        int $year,
        int $hour = 12,
        int $minutes = 0,
        int $seconds = 0,
    ): ReceiverStruct
    {
        try {
            $date = new DateTime(sprintf('%s.%s.%s %s:%s:%s', $day, $month, $year, $hour, $minutes, $seconds));

            $this->registered = $date;
        } catch (Exception $e) {
            $this->registered = new DateTime();
        }

        return $this;
    }

    /**
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int $hour
     * @param int $minutes
     * @param int $seconds
     * @return ReceiverStruct
     */
    public function setActivatedByReadableDate(
        int $day,
        int $month,
        int $year,
        int $hour = 12,
        int $minutes = 0,
        int $seconds = 0,
    ): ReceiverStruct
    {
        try {
            $date = new DateTime(sprintf('%s.%s.%s %s:%s:%s', $day, $month, $year, $hour, $minutes, $seconds));

            $this->activated = $date;
        } catch (Exception $e) {
            $this->activated = new DateTime();
        }

        return $this;
    }

    /**
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int $hour
     * @param int $minutes
     * @param int $seconds
     * @return ReceiverStruct
     */
    public function setDeactivatedByReadableDate(
        int $day,
        int $month,
        int $year,
        int $hour = 12,
        int $minutes = 0,
        int $seconds = 0,
    ): ReceiverStruct
    {
        try {
            $date = new DateTime(sprintf('%s.%s.%s %s:%s:%s', $day, $month, $year, $hour, $minutes, $seconds));

            $this->deactivated = $date;
        } catch (Exception $e) {
            $this->deactivated = new DateTime();
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        $data = [
            'email' => $this->email
        ];

        if ($this->registered) {
            $data['registered'] = (string)$this->registered->getTimestamp();
        }

        if ($this->activated) {
            $data['activated'] = (string)$this->activated->getTimestamp();
        }

        if ($this->deactivated) {
            $data['deactivated'] = (string)$this->deactivated?->getTimestamp();
        }

        if ($this->source) {
            $data['source'] = $this->source;
        }

        if (count($this->attributes)) {
            $data['attributes'] = $this->attributes;
        }

        if (count($this->globalAttributes)) {
            $data['global_attributes'] = $this->globalAttributes;
        }

        if (count($this->tags)) {
            $data['tags'] = $this->tags;
        }

        if (count($this->orders)) {
            $data['orders'] = array_map(fn (OrderStruct $order) => $order->toArray(), $this->orders);
        }

        return $data;
    }
}
