<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:45
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class MailingStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class MailingStruct implements StructInterface
{

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var string|null
     */
    private ?string $subject = null;

    /**
     * @var string|null
     */
    private ?string $senderName = null;

    /**
     * @var string|null
     */
    private ?string $senderEmail = null;

    /**
     * @var MailingContentStruct
     */
    public MailingContentStruct $content;

    /**
     * @var MailingReceiverStruct
     */
    public MailingReceiverStruct $receivers;

    /**
     * @var MailingSettingStruct
     */
    public MailingSettingStruct $settings;

    /**
     * @var array
     */
    private array $tags = [];

    public function __construct()
    {
        $this->content = new MailingContentStruct();
        $this->receivers = new MailingReceiverStruct();
        $this->settings = new MailingSettingStruct();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): MailingStruct
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string|null $subject
     * @return $this
     */
    public function setSubject(?string $subject): MailingStruct
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderName(): ?string
    {
        return $this->senderName;
    }

    /**
     * @param string|null $senderName
     * @return $this
     */
    public function setSenderName(?string $senderName): MailingStruct
    {
        $this->senderName = $senderName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderEmail(): ?string
    {
        return $this->senderEmail;
    }

    /**
     * @param string|null $senderEmail
     * @return $this
     */
    public function setSenderEmail(?string $senderEmail): MailingStruct
    {
        $this->senderEmail = $senderEmail;
        return $this;
    }

    /**
     * @return MailingContentStruct
     */
    public function getContent(): MailingContentStruct
    {
        return $this->content;
    }

    /**
     * @param MailingContentStruct $content
     * @return $this
     */
    public function setContent(MailingContentStruct $content): MailingStruct
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return MailingReceiverStruct
     */
    public function getReceivers(): MailingReceiverStruct
    {
        return $this->receivers;
    }

    /**
     * @param MailingReceiverStruct $receivers
     * @return $this
     */
    public function setReceivers(MailingReceiverStruct $receivers): MailingStruct
    {
        $this->receivers = $receivers;
        return $this;
    }

    /**
     * @return MailingSettingStruct
     */
    public function getSettings(): MailingSettingStruct
    {
        return $this->settings;
    }

    /**
     * @param MailingSettingStruct $settings
     * @return $this
     */
    public function setSettings(MailingSettingStruct $settings): MailingStruct
    {
        $this->settings = $settings;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return $this
     */
    public function setTags(array $tags): MailingStruct
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function addTag(string $tag): MailingStruct
    {
        $this->tags[] = $tag;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $content = $this->content->toArray();
        $receivers = $this->receivers->toArray();
        $settings = $this->settings->toArray();

        $data = [];

        if ($this->name) {
            $data['name'] = $this->name;
        }

        if ($this->subject) {
            $data['subject'] = $this->subject;
        }

        if ($this->senderName) {
            $data['senderer_name'] = $this->senderName;
        }

        if ($this->senderEmail) {
            $data['senderer_email'] = $this->senderEmail;
        }

        if (!empty($content)) {
            $data['content'] = $content;
        }

        if (!empty($receivers)) {
            $data['receivers'] = $receivers;
        }

        if (!empty($settings)) {
            $data['settings'] = $settings;
        }

        if (!empty($this->tags)) {
            $data['tags'] = $this->tags;
        }

        return $data;
    }
}
