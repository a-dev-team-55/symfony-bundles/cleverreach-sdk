<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 27.11.2023 00:53
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

/**
 * @class ReceiverList
 * @package ADevTeam\CleverReachBundle\Struct
 */
class ReceiverList
{

    /**
     * @var string[]
     */
    protected array $receivers = [];

    /**
     * @return string[]
     */
    public function getReceivers(): array
    {
        return $this->receivers;
    }

    /**
     * @param array $receivers
     * @return $this
     */
    public function setReceivers(array $receivers): self
    {
        $this->receivers = $receivers;
        return $this;
    }

    /**
     * @param int|string $receiverId
     * @return $this
     */
    public function addReceiverId(int|string $receiverId): self
    {
        $this->receivers[] = (string)$receiverId;
        return $this;
    }

    /**
     * @param string $receiverId
     * @return $this
     */
    public function addReceiverMail(string $receiverId): self
    {
        $this->receivers[] = $receiverId;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'receivers' => $this->receivers
        ];
    }
}
