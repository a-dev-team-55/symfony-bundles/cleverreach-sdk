<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:51
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\AttributeType;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class UpdateAttribute
 * @package ADevTeam\CleverReachBundle\Struct
 */
class UpdateAttributeStruct implements StructInterface
{

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var AttributeType|null
     */
    private ?AttributeType $type = null;

    /**
     * @var string|null
     */
    private ?string $description;

    /**
     * @var string|null
     */
    private ?string $previewValue;

    /**
     * @var string|null
     */
    private ?string $defaultValue;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): UpdateAttributeStruct
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return AttributeType|null
     */
    public function getType(): ?AttributeType
    {
        return $this->type;
    }

    /**
     * @param AttributeType|null $type
     * @return $this
     */
    public function setType(?AttributeType $type): UpdateAttributeStruct
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): UpdateAttributeStruct
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPreviewValue(): ?string
    {
        return $this->previewValue;
    }

    /**
     * @param string|null $previewValue
     * @return $this
     */
    public function setPreviewValue(?string $previewValue): UpdateAttributeStruct
    {
        $this->previewValue = $previewValue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    /**
     * @param string|null $defaultValue
     * @return $this
     */
    public function setDefaultValue(?string $defaultValue): UpdateAttributeStruct
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if ($this->name) {
            $data['name'] = $this->name;
        }

        if ($this->type) {
            $data['type'] = $this->type->value;
        }

        if ($this->description) {
            $data['description'] = $this->description;
        }

        if ($this->previewValue) {
            $data['previewValue'] = $this->previewValue;
        }

        if ($this->defaultValue) {
            $data['defaultValue'] = $this->defaultValue;
        }

        return $data;
    }
}
