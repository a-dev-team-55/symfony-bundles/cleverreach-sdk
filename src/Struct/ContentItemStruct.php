<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:16
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\ContentItemType;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class ContentItemStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class ContentItemStruct implements StructInterface
{

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var string|null
     */
    private ?string $url = null;

    /**
     * @var string|null
     */
    private ?string $password = null;

    /**
     * @var ContentItemType|null
     */
    private ?ContentItemType $type = null;

    /**
     * @var string|null
     */
    private ?string $cors = null;

    /**
     * @var string|null
     */
    private ?string $icon = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ContentItemStruct
    {
        $this->name = $name;
        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): ContentItemStruct
    {
        $this->url = $url;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): ContentItemStruct
    {
        $this->password = $password;
        return $this;
    }

    public function getType(): ?ContentItemType
    {
        return $this->type;
    }

    public function setType(?ContentItemType $type): ContentItemStruct
    {
        $this->type = $type;
        return $this;
    }

    public function getCors(): ?string
    {
        return $this->cors;
    }

    public function setCors(?string $cors): ContentItemStruct
    {
        $this->cors = $cors;
        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): ContentItemStruct
    {
        $this->icon = $icon;
        return $this;
    }


    public function toArray(): array
    {
        $data = [
            'name' => $this->name,
            'url' => $this->url
        ];

        if ($this->password) {
            $data['password'] = $this->password;
        }

        if ($this->type instanceof ContentItemType) {
            $data['type'] = $this->type->value;
        }

        if ($this->cors) {
            $data['cors'] = $this->password;
        }

        if ($this->icon) {
            $data['icon'] = $this->password;
        }

        return $data;
    }
}
