<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:51
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\AttributeType;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class AttributeStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class AttributeStruct implements StructInterface
{

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var AttributeType|null
     */
    private ?AttributeType $type = null;

    /**
     * @var string|null
     */
    private ?string $groupId = null;

    /**
     * @var string|null
     */
    private ?string $description = null;

    /**
     * @var string|null
     */
    private ?string $previewValue = null;

    /**
     * @var string|null
     */
    private ?string $defaultValue = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): AttributeStruct
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return AttributeType
     */
    public function getType(): AttributeType
    {
        return $this->type;
    }

    /**
     * @param AttributeType $type
     * @return $this
     */
    public function setType(AttributeType $type): AttributeStruct
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    /**
     * @param string|null $groupId
     * @return $this
     */
    public function setGroupId(?string $groupId): AttributeStruct
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): AttributeStruct
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPreviewValue(): ?string
    {
        return $this->previewValue;
    }

    /**
     * @param string|null $previewValue
     * @return $this
     */
    public function setPreviewValue(?string $previewValue): AttributeStruct
    {
        $this->previewValue = $previewValue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    /**
     * @param string|null $defaultValue
     * @return $this
     */
    public function setDefaultValue(?string $defaultValue): AttributeStruct
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        if ($this->name !== null) {
            $data['name'] = $this->name;
        }

        if ($this->type !== null) {
            $data['type'] = $this->type->value;
        }

        if ($this->groupId !== null) {
            $data['groupId'] = $this->groupId;
        }

        if ($this->description !== null) {
            $data['description'] = $this->description;
        }

        if ($this->previewValue !== null) {
            $data['previewValue'] = $this->previewValue;
        }

        if ($this->defaultValue !== null) {
            $data['defaultValue'] = $this->defaultValue;
        }

        return $data;
    }
}
