<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:50
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\MailingSettingEditor;
use ADevTeam\CleverReachBundle\Constant\MailingSettingLinkTrackingType;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class MailingSettingStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class MailingSettingStruct implements StructInterface
{

    /**
     * @var MailingSettingEditor|null
     */
    private ?MailingSettingEditor $editor = null;

    /**
     * @var bool
     */
    private bool $openTracking = true;

    /**
     * @var bool
     */
    private bool $clickRating = true;

    /**
     * @var string|null
     */
    private ?string $linkTrackingUrl = null;

    /**
     * @var MailingSettingLinkTrackingType|null
     */
    private ?MailingSettingLinkTrackingType $linkTrackingType;

    /**
     * @var string|null
     */
    private ?string $googleCampaignName = null;

    /**
     * @var int|null
     */
    private ?int $unsubscribeFormId = null;

    /**
     * @var int|null
     */
    private ?int $categoryId = null;

    /**
     * @return MailingSettingEditor
     */
    public function getEditor(): MailingSettingEditor
    {
        return $this->editor;
    }

    /**
     * @return bool
     */
    public function isOpenTracking(): bool
    {
        return $this->openTracking;
    }

    /**
     * @param bool $openTracking
     * @return $this
     */
    public function setOpenTracking(bool $openTracking): MailingSettingStruct
    {
        $this->openTracking = $openTracking;
        return $this;
    }

    /**
     * @return bool
     */
    public function isClickRating(): bool
    {
        return $this->clickRating;
    }

    /**
     * @param bool $clickRating
     * @return $this
     */
    public function setClickRating(bool $clickRating): MailingSettingStruct
    {
        $this->clickRating = $clickRating;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkTrackingUrl(): ?string
    {
        return $this->linkTrackingUrl;
    }

    /**
     * @param string|null $linkTrackingUrl
     * @return $this
     */
    public function setLinkTrackingUrl(?string $linkTrackingUrl): MailingSettingStruct
    {
        $this->linkTrackingUrl = $linkTrackingUrl;
        return $this;
    }

    /**
     * @return MailingSettingLinkTrackingType|null
     */
    public function getLinkTrackingType(): ?MailingSettingLinkTrackingType
    {
        return $this->linkTrackingType;
    }

    /**
     * @param MailingSettingLinkTrackingType|null $linkTrackingType
     * @return $this
     */
    public function setLinkTrackingType(?MailingSettingLinkTrackingType $linkTrackingType): MailingSettingStruct
    {
        $this->linkTrackingType = $linkTrackingType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGoogleCampaignName(): ?string
    {
        return $this->googleCampaignName;
    }

    /**
     * @param string|null $googleCampaignName
     * @return $this
     */
    public function setGoogleCampaignName(?string $googleCampaignName): MailingSettingStruct
    {
        $this->googleCampaignName = $googleCampaignName;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUnsubscribeFormId(): ?int
    {
        return $this->unsubscribeFormId;
    }

    /**
     * @param int|null $unsubscribeFormId
     * @return $this
     */
    public function setUnsubscribeFormId(?int $unsubscribeFormId): MailingSettingStruct
    {
        $this->unsubscribeFormId = $unsubscribeFormId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     * @return $this
     */
    public function setCategoryId(?int $categoryId): MailingSettingStruct
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if ($this->editor) {
            $data['editor'] = $this->editor->value;
        }

        if ($this->openTracking) {
            $data['open_tracking'] = $this->openTracking;
        }

        if ($this->clickRating) {
            $data['click_rating'] = $this->clickRating;
        }

        if ($this->linkTrackingUrl) {
            $data['link_tracking_url'] = $this->linkTrackingUrl;
        }

        if ($this->linkTrackingType) {
            $data['link_tracking_type'] = $this->linkTrackingType->value;
        }

        if ($this->googleCampaignName) {
            $data['google_campaign_name'] = $this->googleCampaignName;
        }

        if ($this->unsubscribeFormId) {
            $data['unsubscribe_form_id'] = $this->unsubscribeFormId;
        }

        if ($this->categoryId) {
            $data['category_id'] = $this->categoryId;
        }

        return $data;
    }
}
