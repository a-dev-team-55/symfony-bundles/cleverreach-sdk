<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:05
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\FilterDetailDepth;
use ADevTeam\CleverReachBundle\Constant\OrderDirection;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class FilterStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class ReceiverFilterStruct implements StructInterface
{

    /**
     * @var string[]
     */
    private array $groupIds = [];

    /**
     * @var FilterOperatorStruct
     */
    private FilterOperatorStruct $operator = FilterOperatorStruct::OR;

    /**
     * @var string|null
     */
    private ?string $orderBy = null;

    /**
     * @var OrderDirection
     */
    private OrderDirection $orderDirection = OrderDirection::DESC;

    /**
     * @var FilterDetailDepth
     */
    private FilterDetailDepth $detailDepth = FilterDetailDepth::NONE;

    /**
     * @var int
     */
    private int $pageSize = 250;

    /**
     * @var int
     */
    private int $page = 0;

    /**
     * @var FilterRuleStruct[]
     */
    private array $rules = [];

    /**
     * @return string[]
     */
    public function getGroupIds(): array
    {
        return $this->groupIds;
    }

    /**
     * @param array $groupIds
     * @return $this
     */
    public function setGroupIds(array $groupIds): ReceiverFilterStruct
    {
        $this->groupIds = $groupIds;
        return $this;
    }

    /**
     * @param string|int $groupId
     * @return $this
     */
    public function addGroupId(string|int $groupId): ReceiverFilterStruct
    {
        $this->groupIds[] = (string)$groupId;
        return $this;
    }

    /**
     * @return FilterOperatorStruct
     */
    public function getOperator(): FilterOperatorStruct
    {
        return $this->operator;
    }

    /**
     * @param FilterOperatorStruct $operator
     * @return $this
     */
    public function setOperator(FilterOperatorStruct $operator): ReceiverFilterStruct
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    /**
     * @param string|null $orderBy
     * @return $this
     */
    public function setOrderBy(?string $orderBy): ReceiverFilterStruct
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @return OrderDirection
     */
    public function getOrderDirection(): OrderDirection
    {
        return $this->orderDirection;
    }

    /**
     * @param OrderDirection $orderDirection
     * @return $this
     */
    public function setOrderDirection(OrderDirection $orderDirection): ReceiverFilterStruct
    {
        $this->orderDirection = $orderDirection;
        return $this;
    }

    /**
     * @return FilterDetailDepth
     */
    public function getDetailDepth(): FilterDetailDepth
    {
        return $this->detailDepth;
    }

    /**
     * @param FilterDetailDepth $detailDepth
     * @return $this
     */
    public function setDetailDepth(FilterDetailDepth $detailDepth): ReceiverFilterStruct
    {
        $this->detailDepth = $detailDepth;
        return $this;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     * @return $this
     */
    public function setPageSize(int $pageSize): ReceiverFilterStruct
    {
        $this->pageSize = $pageSize;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): ReceiverFilterStruct
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return FilterRuleStruct[]
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param FilterRuleStruct[] $rules
     * @return $this
     */
    public function setRules(array $rules): ReceiverFilterStruct
    {
        $this->rules = $rules;
        return $this;
    }

    /**
     * @param FilterRuleStruct $rule
     * @return $this
     */
    public function addRule(FilterRuleStruct $rule): ReceiverFilterStruct
    {
        $this->rules[] = $rule;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'groups' => $this->groupIds,
            'operator' => $this->operator->value,
            'rules' => array_map(fn(FilterRuleStruct $r) => $r->toArray(), $this->rules),
            'pagesize' => $this->pageSize,
            'page' => $this->page,
            'detail' => $this->detailDepth->value
        ];

        if ($this->orderBy) {
            $data['orderby'] = sprintf('%s %s', $this->orderBy, $this->orderDirection->value);
        }

        return $data;
    }
}
