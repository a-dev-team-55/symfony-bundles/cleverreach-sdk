<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 21:48
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * @class ReceiverOrderStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class ReceiverOrderStruct implements StructInterface
{

    /**
     * @var string
     */
    private string $orderId;

    /**
     * @var string
     */
    private string $productName;

    /**
     * @var string|null
     */
    private ?string $groupId = null;

    /**
     * @var string|null
     */
    private ?string $productId = null;

    /**
     * @var DateTimeInterface|int|null
     */
    private int|null|DateTimeInterface $stamp = null;

    /**
     * @var int
     */
    private int $price = 0;

    /**
     * @var int
     */
    private int $quantity = 1;

    /**
     * @var string|null
     */
    private ?string $mailingId = null;

    /**
     * @var string|null
     */
    private ?string $source = null;

    /**
     * @var string
     */
    private string $currency = 'EUR';

    /**
     * @param string $orderId
     * @param string $productName
     */
    public function __construct(string $orderId, string $productName)
    {
        $this->orderId = $orderId;
        $this->productName = $productName;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @param string $orderId
     * @return $this
     */
    public function setOrderId(string $orderId): ReceiverOrderStruct
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     * @return $this
     */
    public function setProductName(string $productName): ReceiverOrderStruct
    {
        $this->productName = $productName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    /**
     * @param string|null $groupId
     * @return $this
     */
    public function setGroupId(?string $groupId): ReceiverOrderStruct
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param string|null $productId
     * @return $this
     */
    public function setProductId(?string $productId): ReceiverOrderStruct
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * @return DateTimeInterface|int|null
     */
    public function getStamp(): DateTimeInterface|int|null
    {
        return $this->stamp;
    }

    /**
     * @param DateTimeInterface|int|null $stamp
     * @return $this
     */
    public function setStamp(DateTimeInterface|int|null $stamp): ReceiverOrderStruct
    {
        $this->stamp = $stamp;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return $this
     */
    public function setPrice(int $price): ReceiverOrderStruct
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(int $quantity): ReceiverOrderStruct
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMailingId(): ?string
    {
        return $this->mailingId;
    }

    /**
     * @param string|null $mailingId
     * @return $this
     */
    public function setMailingId(?string $mailingId): ReceiverOrderStruct
    {
        $this->mailingId = $mailingId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     * @return $this
     */
    public function setSource(?string $source): ReceiverOrderStruct
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency(string $currency): ReceiverOrderStruct
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int $hour
     * @param int $minutes
     * @param int $seconds
     * @return ReceiverOrderStruct
     */
    public function setStampByReadableDate(
        int $day,
        int $month,
        int $year,
        int $hour = 12,
        int $minutes = 0,
        int $seconds = 0,
    ): ReceiverOrderStruct
    {
        try {
            $date = new DateTime(sprintf('%s.%s.%s %s:%s:%s', $day, $month, $year, $hour, $minutes, $seconds));

            $this->stamp = $date;
        } catch (Exception $e) {
            $this->stamp = new DateTime();
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'order_id' => $this->orderId,
            'product_name' => $this->productName,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'currency' => $this->currency,
        ];

        if ($this->groupId) {
            $data['group_id'] = $this->groupId;
        }

        if ($this->productId) {
            $data['product_id'] = $this->productId;
        }

        if ($this->stamp) {
            $data['stamp'] = (string)$this->stamp->getTimestamp();
        }

        if ($this->mailingId) {
            $data['mailing_id'] = $this->mailingId;
        }

        if ($this->source) {
            $data['source'] = $this->source;
        }

        return $data;
    }
}
