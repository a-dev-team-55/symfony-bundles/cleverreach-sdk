<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:11
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Constant\FilterField;
use ADevTeam\CleverReachBundle\Constant\FilterLogic;
use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class FilterRule
 * @package ADevTeam\CleverReachBundle\Struct
 */
class FilterRuleStruct implements StructInterface
{

    /**
     * @var FilterField|string
     */
    private FilterField|string $field;

    /**
     * @var FilterLogic
     */
    private FilterLogic $logic;

    /**
     * @var string|int
     */
    private string|int $condition;

    /**
     * @param string $field
     * @param FilterLogic $logic
     * @param int|string $condition
     */
    public function __construct(string $field, FilterLogic $logic, int|string $condition)
    {
        $this->field = $field;
        $this->logic = $logic;
        $this->condition = $condition;
    }

    /**
     * @return FilterField|string
     */
    public function getField(): FilterField|string
    {
        return $this->field instanceof FilterField ? $this->field->value : $this->field;
    }

    /**
     * @param FilterField|string $field
     * @return $this
     */
    public function setField(FilterField|string $field): FilterRuleStruct
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return FilterLogic
     */
    public function getLogic(): FilterLogic
    {
        return $this->logic;
    }

    /**
     * @param FilterLogic $logic
     * @return $this
     */
    public function setLogic(FilterLogic $logic): FilterRuleStruct
    {
        $this->logic = $logic;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getCondition(): int|string
    {
        return $this->condition;
    }

    /**
     * @param int|string $condition
     * @return $this
     */
    public function setCondition(int|string $condition): FilterRuleStruct
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'field' => $this->field instanceof FilterField ? $this->field->value : $this->field,
            'logic' => $this->logic->value,
            'condition' => $this->condition,
        ];
    }
}
