<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 17:49
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class OrderStruct
 * @package ADevTeam\CleverReachBundle\Interface
 */
class OrderStruct implements StructInterface
{

    /**
     * @var string
     */
    private string $orderId;

    /**
     * @var string
     */
    private string $productId;

    /**
     * @var string
     */
    private string $product;

    /**
     * @var float|int
     */
    private float|int $price = 0;

    /**
     * @var string
     */
    private string $currency = 'EUR';

    /**
     * @var int
     */
    private int $amount = 1;

    /**
     * @var string|null
     */
    private ?string $mailingId = null;

    /**
     * @param string $orderId
     * @param string $productId
     * @param string $product
     * @param float|int $price
     */
    public function __construct(string $orderId, string $productId, string $product, float|int $price)
    {
        $this->orderId = $orderId;
        $this->productId = $productId;
        $this->product = $product;
        $this->price = $price;
    }

    /**
     * @param string $currency
     * @return OrderStruct
     */
    public function setCurrency(string $currency): OrderStruct
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @param int $amount
     * @return OrderStruct
     */
    public function setAmount(int $amount): OrderStruct
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @param string|null $mailingId
     * @return OrderStruct
     */
    public function setMailingId(?string $mailingId): OrderStruct
    {
        $this->mailingId = $mailingId;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'order_id' => $this->orderId,
            'product_id' => $this->productId,
            'product' => $this->product,
            'price' => $this->price,
            'currency' => $this->currency,
            'amount' => $this->amount
        ];

        if ($this->mailingId!== null) {
            $data['mailing_id'] = $this->mailingId;
        }

        return $data;
    }
}
