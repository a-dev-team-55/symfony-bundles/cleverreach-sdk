<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 25.11.2023 18:26
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Struct;

use ADevTeam\CleverReachBundle\Interface\StructInterface;

/**
 * @class GroupStruct
 * @package ADevTeam\CleverReachBundle\Struct
 */
class GroupStruct implements StructInterface
{

    /**
     * @var string
     */
    private string $name;

    /**
     * @var bool
     */
    private bool $locked = false;

    /**
     * @var bool
     */
    private bool $backup = false;

    /**
     * @var string|null
     */
    private ?string $description = null;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function setName(?string $name): GroupStruct
    {
        $this->name = $name;
        return $this;
    }

    public function setLocked(bool $locked): GroupStruct
    {
        $this->locked = $locked;
        return $this;
    }

    public function setBackup(bool $backup): GroupStruct
    {
        $this->backup = $backup;
        return $this;
    }

    public function setDescription(?string $description): GroupStruct
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        $data = [
            'name' => $this->name,
            'locked' => $this->locked,
            'backup' => $this->backup,
        ];

        if ($this->description) {
            $data['receiver_info'] = $this->description;
        }

        return $data;
    }
}
