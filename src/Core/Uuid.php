<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:31
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Core;

use ADevTeam\CleverReachBundle\Exception\InvalidUuidException;
use ADevTeam\CleverReachBundle\Exception\InvalidUuidLengthException;
use Exception;

/**
 * @class Uuid
 * @package ADevTeam\CleverReachBundle\Core
 */
abstract class Uuid
{

    /**
     * @throws InvalidUuidException
     * @throws InvalidUuidLengthException
     */
    private static function fromBytesToHex(string $bytes): string
    {
        if (mb_strlen($bytes, '8bit') !== 16) {
            throw new InvalidUuidLengthException(mb_strlen($bytes, '8bit'), bin2hex($bytes));
        }
        $uuid = bin2hex($bytes);

        if (!self::isValid($uuid)) {
            throw new InvalidUuidException($uuid);
        }

        return $uuid;
    }

    /**
     * @param string $id
     * @return bool
     */
    private static function isValid(string $id): bool
    {
        if (!preg_match('/^[0-9a-f]{32}$/', $id)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function fromStringToHex(string $string): string
    {
        try {
            return self::fromBytesToHex(md5($string, true));
        } catch (InvalidUuidException|InvalidUuidLengthException $e) {
            return '';
        }
    }

}
