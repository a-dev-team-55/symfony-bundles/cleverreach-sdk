<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 18:14
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3;

use ADevTeam\CleverReachBundle\Client\V3\Partial\AttributePartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\BlacklistPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\BouncePartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\ClientPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\ContentPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\DebugPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\HookPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\MailingPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\OAuthPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\ReceiverPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\ReportPartial;
use ADevTeam\CleverReachBundle\Interface\AttributePartialInterface;
use ADevTeam\CleverReachBundle\Interface\BlacklistPartialInterface;
use ADevTeam\CleverReachBundle\Interface\BouncePartialInterface;
use ADevTeam\CleverReachBundle\Interface\ClientPartialInterface;
use ADevTeam\CleverReachBundle\Interface\ContentPartialInterface;
use ADevTeam\CleverReachBundle\Interface\DebugPartialInterface;
use ADevTeam\CleverReachBundle\Interface\FormPartialInterface;
use ADevTeam\CleverReachBundle\Interface\GroupPartialInterface;
use ADevTeam\CleverReachBundle\Interface\HookPartialInterface;
use ADevTeam\CleverReachBundle\Interface\MailingPartialInterface;
use ADevTeam\CleverReachBundle\Interface\OAuthPartialInterface;
use ADevTeam\CleverReachBundle\Interface\ReceiverPartialInterface;
use ADevTeam\CleverReachBundle\Interface\ReportPartialInterface;
use BadMethodCallException;
use ADevTeam\CleverReachBundle\Client\V3\Partial\FormPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\GroupPartial;
use GuzzleHttp\Exception\GuzzleException;

/**
 * CleverReach Restclient V3
 * @see https://rest.cleverreach.com/explorer/v3
 *
 * @method FormPartialInterface forms()
 * @method GroupPartialInterface groups()
 * @method ReceiverPartialInterface receivers()
 * @method HookPartialInterface hooks()
 * @method AttributePartialInterface attributes()
 * @method BlacklistPartialInterface blacklists()
 * @method ClientPartialInterface clients()
 * @method ContentPartialInterface content()
 * @method MailingPartialInterface mailings()
 * @method OAuthPartialInterface oauth()
 * @method ReportPartialInterface reports()
 * @method BouncePartialInterface bounced()
 * @method DebugPartialInterface debug()
 *
 * @class CleverReachClient
 * @package ADevTeam\CleverReachBundle\Client\V3
 * @extends AbstractCleverReachClient
 */
class CleverReachClient extends AbstractCleverReachClient
{

    public const ALLOWED_METHODS = [
        'forms' => FormPartial::class,
        'groups' => GroupPartial::class,
        'receivers' => ReceiverPartial::class,
        'hooks' => HookPartial::class,
        'attributes' => AttributePartial::class,
        'blacklists' => BlacklistPartial::class,
        'clients' => ClientPartial::class,
        'content' => ContentPartial::class,
        'mailings' => MailingPartial::class,
        'oauth' => OAuthPartial::class,
        'reports' => ReportPartial::class,
        'bounced' => BouncePartial::class,
        'debug' => DebugPartial::class
    ];


    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws GuzzleException
     */
    public function __call(string $name, array $arguments)
    {
        $name = strtolower($name);
        $headers = $this->getHeaders();

        if (array_key_exists($name, self::ALLOWED_METHODS)) {
            $this->authenticate();

            return new (self::ALLOWED_METHODS[$name])(
                $this->client,
                $this->logger,
                $this->requestLogger,
                $this->responseLogger,
                $headers,
                $this->env,
                $this->batchSize
            );
        }

        throw new BadMethodCallException(
            sprintf('Method %s not found', $name)
        );
    }
}
