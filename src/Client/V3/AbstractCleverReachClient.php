<?php /*
 * @author ADevTeam
 * @created 15.11.2023 17:49
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */ /*
 * @author ADevTeam
 * @created 15.11.2023 18:14
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */ /** @noinspection PhpDuplicateCatchBodyInspection */
declare(strict_types=1);

namespace ADevTeam\CleverReachBundle\Client\V3;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\TransferStats;
use Psr\Cache\InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * @class AbstractCleverReachClient
 * @package ADevTeam\CleverReachBundle\Client\V3
 */
abstract class AbstractCleverReachClient
{
    /**
     * @var string
     */
    protected const TOKEN_URL = 'https://rest.cleverreach.com/oauth/token.php';

    /**
     * @var string
     */
    private const ACCESS_KEY_CACHE = 'clever_reach.token';

    /**
     * @var string
     */
    private const EXPIRATION_CACHE = 'clever_reach.expiration';

    /**
     * @var string
     */
    private const SCOPE_CACHE = 'clever_reach.scopes';

    /**
     * @var int
     */
    protected int $expiresAt = 0;

    /**
     * @var string $accessToken
     */
    protected string $accessToken = '';

    /**
     * @var string $scopes
     */
    protected array $scopes = [];

    /**
     * @var string|null $clientId
     */
    private ?string $clientId = null;

    /**
     * @var string|null $client
     */
    private ?string $clientSecret = null;

    /**
     * @var int $batchSize
     */
    protected int $batchSize = 50;

    /**
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     * @param LoggerInterface $requestLogger
     * @param LoggerInterface $responseLogger
     * @param FilesystemAdapter $cache
     * @param string $env
     * @param string $cacheDir
     * @param bool $caching
     * @param bool $debug
     * @param int $ttl
     */
    public function __construct(
        protected ClientInterface $client,
        protected readonly LoggerInterface $logger,
        protected readonly LoggerInterface $requestLogger,
        protected readonly LoggerInterface $responseLogger,
        protected readonly FilesystemAdapter $cache,
        protected readonly string $env,
        protected readonly string $cacheDir,
        protected readonly bool $caching,
        protected readonly bool $debug,
        protected readonly int $ttl,
    ) {
        try {
            $this->accessToken = $this->cache->getItem(self::ACCESS_KEY_CACHE)?->get() ?? '';
            $this->expiresAt = $this->cache->getItem(self::EXPIRATION_CACHE)?->get() ?? 0;
            $this->scopes = $this->cache->getItem(self::SCOPE_CACHE)?->get() ?? [];
        } catch (InvalidArgumentException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param int $batchSize
     * @return $this
     */
    public function setBatchSize(int $batchSize): AbstractCleverReachClient
    {
        $this->batchSize = $batchSize;
        return $this;
    }

    /**
     * @param string $clientId
     * @return $this
     */
    public function setClientId(string $clientId): AbstractCleverReachClient
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @param string $clientSecret
     * @return $this
     */
    public function setClientSecret(string $clientSecret): AbstractCleverReachClient
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    /**
     * @return string[]
     */
    protected function getHeaders(): array
    {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        if (!empty($this->accessToken)) {
            $headers['authorization'] = 'Bearer '. $this->accessToken;
        }

        return $headers;
    }

    /**
     * @return bool
     * @throws GuzzleException
     */
    public function authenticate(): bool
    {
        if (empty($this->accessToken)) {
            $options = [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->clientId,
                    'client_secret' => $this->clientSecret
                ],
                'on_stats' => function (TransferStats $stats) use (&$request) {
                    $request = $stats->getRequest();
                }
            ];


            try {
                /** @var ResponseInterface $response */
                $response = $this->client->post(
                    self::TOKEN_URL,
                    $options
                );

                $response->getBody()->rewind();

                $content = json_decode($response->getBody()->getContents(), true);

                $maskedBody = Utils::streamFor(
                    json_encode(
                        array_merge(
                            $content,
                            [
                                'access_token' => str_repeat('*', strlen($content['access_token'] ?? '*'))
                            ]
                        )
                    )
                );

                $maskedResponse = new Response(
                    $response->getStatusCode(),
                    $response->getHeaders(),
                    $maskedBody
                );

                new CleverReachResponse($maskedResponse, $request, $this->requestLogger, $this->responseLogger);

                $accessToken = $content['access_token'] ?? '';
                $expiresAt = time() + (int)$content['expires_in'] ?? (time() * -1);
                $scopes = array_map(fn($v) => ltrim($v, 'oa_'), explode(' ', $content['scope']));

                if (!empty($accessToken) && $expiresAt > 0) {
                    $this->accessToken = $this->setAccessToken($accessToken, $expiresAt);
                    $this->expiresAt = $this->setExpiresAt($expiresAt);
                    $this->scopes = $this->setScopes($scopes, $expiresAt);

                    $this->client = new Client([
                        'headers' => [
                            'Authorization' => 'Bearer ' . $this->accessToken
                        ]
                    ]);

                    return true;
                }
            } catch (RequestException $e) {
                new CleverReachResponse(
                    $e->getResponse(),
                    $e->getRequest(),
                    $this->requestLogger,
                    $this->responseLogger
                );
            }

            return false;
        }

        $this->client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken
            ]
        ]);

        return true;
    }

    /**
     * @param int|null $expiresAfter
     * @return int
     */
    private function setExpiresAt(?int $expiresAfter = null): int
    {
        try {
            $item = $this->cache->getItem(self::EXPIRATION_CACHE);

            if ($expiresAfter) {
                $item->expiresAfter($expiresAfter);
                $item->set($expiresAfter);
            }

            $this->cache->save($item);

            return $item->get() ?? 0;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        } catch (InvalidArgumentException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return 0;
    }

    /**
     * @param array|null $scopes
     * @param int|null $expiresAfter
     * @return array
     */
    private function setScopes(?array $scopes = null, ?int $expiresAfter = null): array
    {
        try {
            $item = $this->cache->getItem(self::SCOPE_CACHE);

            if ($scopes && $expiresAfter) {
                $item->expiresAfter($expiresAfter);
                $item->set($scopes);
            }

            $this->cache->save($item);

            return $item->get() ?? [];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        } catch (InvalidArgumentException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return [];
    }

    /**
     * @param string|null $accessToken
     * @param int|null $expiresAfter
     * @return string
     */
    private function setAccessToken(string $accessToken = null, int $expiresAfter = null): string
    {
        try {
            $item = $this->cache->getItem(self::ACCESS_KEY_CACHE);

            if ($expiresAfter) {
                $item->expiresAfter($expiresAfter);
            }

            if ($accessToken) {
                $item->set($accessToken);
            }

            $this->cache->save($item);

            return $item->get() ?? '';
        } catch (InvalidArgumentException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return '';
    }

    /**
     * @return array
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }
}
