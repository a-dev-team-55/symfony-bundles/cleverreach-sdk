<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:50
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Constant\ReportState;
use ADevTeam\CleverReachBundle\Constant\StatisticMode;
use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\ReportPartialInterface;
use DateTimeInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class ReportPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class ReportPartial extends AbstractPartial implements ReportPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/reports';

    /**
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $reportIdOrMailingId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOneByGroupId(string $reportIdOrMailingId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $reportIdOrMailingId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $reportIdOrMailingId
     * @param ReportState $state
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceiverInfos(string $reportIdOrMailingId, ReportState $state = ReportState::SENT): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $reportIdOrMailingId, 'receivers', $state->value),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $reportIdOrMailingId
     * @param StatisticMode $mode
     * @param DateTimeInterface|null $start
     * @param DateTimeInterface|null $end
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getStatsByReportId(
        string $reportIdOrMailingId,                // ID of Report/Mailing.
        StatisticMode $mode = StatisticMode::FULL,  // Mode of statistics.
        ?DateTimeInterface $start = null,           // Startdate unixtimestamp (only for mode daily).
        ?DateTimeInterface $end = null              // Enddate unixtimestamp (only for mode daily).
    ): CleverReachResponseInterface
    {
        $options = [
            'on_stats' => $this->getStatHandler($request)
        ];

        if ($start && $end && $mode === StatisticMode::DAILY) {
            $options = [
                'query' => [
                    'start' => $start->getTimestamp(),
                    'end' => $end->getTimestamp()
                ]
            ];
        }

        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $reportIdOrMailingId, 'stats', $mode->value),
                $options
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Delete a certain report
     *
     * @param string $reportIdOrMailingId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $reportIdOrMailingId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $reportIdOrMailingId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
