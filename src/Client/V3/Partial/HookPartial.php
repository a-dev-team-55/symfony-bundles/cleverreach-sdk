<?php
/*
 * @author ADevTeam
 * @created 15.11.2023 19:06
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

/** @noinspection PhpDuplicateCatchBodyInspection */
/** @noinspection DuplicatedCode */
declare(strict_types=1);

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Constant\HookEvent;
use ADevTeam\CleverReachBundle\Core\Uuid;
use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\HookPartialInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class HookPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class HookPartial extends AbstractPartial implements HookPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL . '/hooks/eventhook';

    /**
     * @param string $url
     * @param string $groupId
     * @param HookEvent $event
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function register(string $url, string $groupId, HookEvent $event): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => [
                        'url' => $url,
                        'event' => $event->value,
                        'condition' => $groupId,
                        'verify' => Uuid::fromStringToHex(
                            sprintf(
                                '%s%s',
                                $url, $event->value
                            )
                        )
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $groupId
     * @param HookEvent $event
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $groupId, HookEvent $event): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $event->value),
                [
                    'condition' => $groupId,
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
