<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:50
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Constant\MailingState;
use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\MailingPartialInterface;
use ADevTeam\CleverReachBundle\Struct\MailingStruct;
use DateTimeInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class MailingPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class MailingPartial extends AbstractPartial implements MailingPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/mailings';

    /**
     * returns lists of mailings according to following filter settings
     *
     * NOTE: The start and end fields depend on the state field.
     * - finished mails are filtered by finished time
     * - draft mails are filtered by creation time
     * - waiting mails are filtered by the time they will be sent
     * - running mails are filtered by the time they have been started
     *
     * @param int $limit                        // 	number of items.
     * @param MailingState $state               // state of mailing.
     * @param string|null $channelId            // (optional): if specified, it will only return matching results.
     * @param DateTimeInterface|null $start     // (optional) Filter by start time (see note).
     * @param DateTimeInterface|null $end       // (optional) Filter by end time (see note).
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(
        int $limit = 25,
        MailingState $state = MailingState::ALL,
        ?string $channelId = null,
        ?DateTimeInterface $start = null,
        ?DateTimeInterface $end = null
    ): CleverReachResponseInterface
    {
        $options = [
            'query' => [
                'limit' => $limit,
                'state' => $state->value
            ],
            'on_stats' => $this->getStatHandler($request)
        ];

        if ($channelId) {
            $options['query']['channelId'] = $channelId;
        }

        if ($start && $end) {
            $options['query']['start'] = $start->getTimestamp();
            $options['query']['end'] = $end->getTimestamp();
        }

        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                $options
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get a mailing by id
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOneByMailingId(string $mailingId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $mailingId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get links for a mailing by id
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getLinksByMailingId(string $mailingId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $mailingId, 'links'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return a list of orders
     *
     * @param string $reportIdOrMailingId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOrdersByMailingId(string $reportIdOrMailingId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $reportIdOrMailingId, 'orders'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get random receiver for a mailing for preview reasons
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getRandomReceiverByMailingId(string $mailingId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $mailingId, 'randomreceiver'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * returns array of categories
     *
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getCategories(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, 'channel'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * specific category
     *
     * @param string $categoryId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getCategoryByCategoryId(string $categoryId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, 'channel', $categoryId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * returns agency templates, if there are any
     *
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAgencyTemplates(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, 'templates', 'agency'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * returns user templates, if there are any
     *
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getUserTemplates(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, 'templates', 'user'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * create a new mailing
     *
     * Example Post Data:
     * {
     *   "name": "my internal name",
     *   "subject": "subject line",
     *   "sender_name": "Bruce Wayne (Wayne corp.)",
     *   "sender_email": "bruce.wayne@gotham.com",
     *   "content": {
     *     "type": "html/text",                     // "html", "text" or "html/text"
     *     "html": "<html><body>The two faces of Harvey Dent</body></html>",
     *     "text": "The two faces of Harvey Dent"
     *   },
     *   "receivers": {                             // fill either a list of group ids or a filter id
     *     "groups": [ "31939", "81942" ],
     *     "filter": "66"
     *   },
     *   "settings": {
     *     "editor": "wizard",                      // "wizard", "freeform", "advanced", "plaintext"
     *     "open_tracking": true,                   // track opening of emails
     *     "click_tracking": true,                  // track clicks of emails
     *     "link_tracking_url": "27.wayne.cleverreach.com",
     *     "link_tracking_type": "google",          // "google", "intelliad", "crconnect"
     *     "google_campaign_name": "My Campaign",   // in case of link_tracking_type == "google"
     *     "unsubscribe_form_id": "23",
     *     "category_id": "54"
     *   },
     *   "tags": ["setup_v2"]                       // use "setup_v2" for new mailing editor
     * }
     *
     * Note: For links not to be substituted by tracking links, use 'single quotes' around urls in your html.
     *
     * @param MailingStruct $mailing
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function add(MailingStruct $mailing): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => $mailing->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $mailingId
     * @param string[] $receiverMailAddresses
     * @param string $previewText
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function sendPreview(string $mailingId, array $receiverMailAddresses, string $previewText = ''): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $mailingId, 'sendpreview'),
                [
                    'form_params' => [
                        'receivers' => $receiverMailAddresses,
                        'previewText' => $previewText
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $internalName
     * @param string $html
     * @param string[] $tags
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function createTemplate(string $internalName, string $html, array $tags = []): CleverReachResponseInterface
    {
        $options = [
            'form_params' => [
                'name' => $internalName,
                'html' => $html
            ],
            'on_stats' => $this->getStatHandler($request)
        ];

        if (!empty($tags)) {
            $options['form_params']['tags'] = $tags;
        }

        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, 'template'),
                $options
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * update mailing
     *
     * Example Post Data:
     * {
     *   "name": "my internal name",
     *   "subject": "subject line",
     *   "sender_name": "Bruce Wayne (Wayne corp.)",
     *   "sender_email": "bruce.wayne@gotham.com",
     *   "content": {
     *     "type": "html/text",                     // "html", "text" or "html/text"
     *     "html": "<html><body>The two faces of Harvey Dent</body></html>",
     *     "text": "The two faces of Harvey Dent"
     *   },
     *   "receivers": {                             // fill either a list of group ids or a filter id
     *     "groups": [ "31939", "81942" ],
     *     "filter": "66"
     *   },
     *   "settings": {
     *     "editor": "wizard",                      // "wizard", "freeform", "advanced", "plaintext"
     *     "open_tracking": true,                   // track opening of emails
     *     "click_tracking": true,                  // track clicks of emails
     *     "link_tracking_url": "27.wayne.cleverreach.com",
     *     "link_tracking_type": "google",          // "google", "intelliad", "crconnect"
     *     "google_campaign_name": "My Campaign",   // in case of link_tracking_type == "google"
     *     "unsubscribe_form_id": "23",
     *     "category_id": "54"
     *   },
     *   "tags": ["setup_v2"]                       // use "setup_v2" for new mailing editor
     * }
     *
     * @param string $mailingId
     * @param MailingStruct $mailing
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function update(string $mailingId, MailingStruct $mailing): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $mailingId),
                [
                    'form_params' => $mailing->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Delete a certain report
     *
     * @param string $mailingId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $mailingId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, 'channel', $mailingId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
