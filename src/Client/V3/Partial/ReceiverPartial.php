<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 17.11.2023 21:38
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Constant\ReceiverState;
use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\ReceiverPartialInterface;
use ADevTeam\CleverReachBundle\Struct\CloneReceiverStruct;
use ADevTeam\CleverReachBundle\Struct\DeleteReceiversStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverEventStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverFilterStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverOrderStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverValidationStruct;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class ReceiverPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class ReceiverPartial extends AbstractPartial implements ReceiverPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/receivers';

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOne(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $receiverIdOrMail),
                [
                    'query' => [
                        'group_id' => $groupId,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAttributesByReceiverId(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'attributes'),
                [
                    'query' => [
                        'group_id' => $groupId,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $receiverIdOrMail
     * @param ReceiverState $state
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceiverGroupsByReceiverId(string $receiverIdOrMail, ReceiverState $state = ReceiverState::ALL): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'groups'),
                [
                    'query' => [
                        'state' => $state->value,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getEventsByGroupId(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'events'),
                [
                    'query' => [
                        'group_id' => $groupId,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getBounced(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, 'bounced'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $receiverIdOrMail
     * @param CloneReceiverStruct $newReceiver
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function clone(string $receiverIdOrMail, CloneReceiverStruct $newReceiver): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'clone'),
                [
                    'form_params' => $newReceiver->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $receiverIdOrMail
     * @param ReceiverEventStruct $event
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function addEvent(string $receiverIdOrMail, ReceiverEventStruct $event): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'events'),
                [
                    'form_params' => $event->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $receiverIdOrMail
     * @param ReceiverOrderStruct $order
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function addOrder(string $receiverIdOrMail, ReceiverOrderStruct $order): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'orders'),
                [
                    'form_params' => $order->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Delete receivers by given list of ids of emails
     *
     * Example Post Data:
     * {
     *   "receivers": [
     *     "381940",
     *     "dick.grayson@wayne.com",
     *     "robin@gotham.com",
     *     "3571983"
     *   ],
     *   "group_id": 271939
     * }
     *
     * @param DeleteReceiversStruct $deletableReceivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deleteMultiple(DeleteReceiversStruct $deletableReceivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, 'delete'),
                [
                    'form_params' => $deletableReceivers->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * returns a list of receivers, filtered by a temporary filter
     *
     * Example Post Data:
     * {
     *   "groups": ["1","2","3"],
     *   "operator": "OR",
     *   "rules": [
     *     { "field":"email", "logic":"contains", "condition":"@gotham.com" },
     *     { "field":"activated", "logic":"bg", "condition":"1234567" },
     *     { "field":"firstname", "logic":"eq", "condition":"bruce" },
     *     { "field":"lastname", "logic":"eq", "condition":"gordon" }
     *   ],
     *   "orderby": "activated desc",
     *   "detail": 0,
     *   "pagesize": 250,
     *   "page": 0
     * }
     *
     * General available fields:
     * email - (email of the receiver)
     * source - (import source of the email)
     * imported - (timestamp of receiver import)
     * registered - (timestamp of receiver registration)
     * activated - (timestamp of receiver activation)
     * deactivated - (timestamp of receiver deactivation)
     * bounced - (timestamp of receiver bounce)
     * points - (points of the receiver)
     * last_location - (last location of receiver)
     * last_client - (last mail client used by receiver)
     * crevent_mail_send (logic: eq, condition: mailing_id -> receiver of certain mailing)
     * crevent_mail_open (logic: eq, condition: mailing_id -> opened mail of certain mailing)
     * crevent_mail_click (logic: eq, condition: mailing_id -> clicked on mail of certain mailing)
     * crevent_mail_bounce (logic: eq, condition: mailing_id -> bounced mail of certain mailing)
     * all attributes (if global and group attributes have the same name, group ones will be favoured)
     *
     * @param ReceiverFilterStruct $filter
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getByTemporaryFilter(ReceiverFilterStruct $filter): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, 'filter'),
                [
                    'form_params' => $filter->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Submit a list of emails and return the valid ones.
     * Returns the not blacklisted emails of given list.
     * If group_id is set, the active state is checked additionally.
     *
     * Example Post Data:
     * {
     *   "emails": [
     *     "joker@arkham.com",
     *     "twoface@dent.com",
     *     "batman@gotham.com"
     *   ],
     *   "group_id": "1939",
     *   "invert": false
     * }
     *
     * @param ReceiverValidationStruct $receivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function validate(ReceiverValidationStruct $receivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, 'isvalid'),
                [
                    'form_params' => $receivers->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Changes the email of a certain receiver
     *
     * @param string $receiverIdOrMail
     * @param string $newEmail
     * @param bool $overwrite
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateEmailByReceiverId(string $receiverIdOrMail, string $newEmail, bool $overwrite = false): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'email'),
                [
                    'form_params' => [
                        'email' => $newEmail,
                        'overwrite' => $overwrite,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * update value of an attribute of a receiver
     *
     * @param string $receiverIdOrMail
     * @param string $attributeId
     * @param mixed $value
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateAttributeByReceiverId(string $receiverIdOrMail, string $attributeId, mixed $value): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'attributes', $attributeId),
                [
                    'form_params' => [
                        'value' => $value
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * updates orderitem
     *
     * @param string $receiverIdOrMail
     * @param string $orderId
     * @param ReceiverOrderStruct $order
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateOrderByReceiverId(string $receiverIdOrMail, string $orderId, ReceiverOrderStruct $order): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $receiverIdOrMail, 'orders', $orderId),
                [
                    'form_params' => $order->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * updates orderitem
     *
     * @param string $receiverIdOrMail
     * @param string|null $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $receiverIdOrMail, ?string $groupId = null): CleverReachResponseInterface
    {
        $options = [
            'on_stats' => $this->getStatHandler($request)
        ];

        if ($groupId) {
            $options['form_params'] = ['group_id' => $groupId];
        }

        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $receiverIdOrMail),
                $options
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * updates orderitem
     *
     * @param string $receiverIdOrMail
     * @param string $orderId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deleteOrder(string $receiverIdOrMail, string $orderId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $receiverIdOrMail),
                [
                    'form_params' => [
                        'id' => $orderId
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
