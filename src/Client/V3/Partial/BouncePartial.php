<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:50
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\BouncePartialInterface;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * @class BouncePartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class BouncePartial extends AbstractPartial implements BouncePartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/bounces';

    /**
     * returns a list with all bounces for your account
     *
     * Categories (exerpt):
     * - permanent/hardbounce: mailbox does not exist
     * - softbounce: temporary errors
     * - blocked: receiving mailserver didn't accept our email
     * - delayed: temporary delivery error
     * - spam: receiving mailserver categorized your email as spam
     *
     * There are also two types of bounces:
     * - type: mailing -- bounce was created through a mailing
     * - type: doi -- bounce was created while sending the DOI emails (form signup)
     * type_id is either the mailing id or the form id depending on the type
     *
     * @param int $page         // Resultpage
     * @param int $pagesize     // max amount of entries per query.
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(int $page = 0, int $pagesize = 500): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                [
                    'query' => [
                        'page' => $page,
                        'pagesize' => $pagesize,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
