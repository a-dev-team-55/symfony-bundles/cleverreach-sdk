<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:49
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\ClientPartialInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class ClientPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class ClientPartial extends AbstractPartial implements ClientPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/clients';

    /**
     * Returns a list of clients managed by this account. In the case of a single client it returns a object
     *
     * @param int $page         // Resultpage
     * @param int $pagesize     // max amount of entries per query.
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(int $page = 0, int $pagesize = 500): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                [
                    'query' => [
                        'page' => $page,
                        'pagesize' => $pagesize,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get info for a single client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOne(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get info for a single client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getActiveReceiverCountByClientId(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId, 'activereceivercount'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get the currently still available contignent of mails, free to send
     * For flatrate this always returns 0.
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getContingentByClientId(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId, 'contingent'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get the current active invoice address for one client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getActiveInvoiceAddressByClientId(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId, 'invoiceaddress'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get the next invoice date as a timestamp
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getNextInvoiceDateByClientId(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId, 'nextinvoicedate'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get subscription/plan info for one client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getSubscriptionPlanByClientId(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId, 'plan'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get the amount of receivers of a certain client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceiverCountByClientId(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId, 'receivercount'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get userlist for a single client
     *
     * @param string $clientId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getUserlistByClientId(string $clientId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $clientId, 'users'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * get info for a single client by a given domain
     *
     * @param string $domain
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getInfoByDomain(string $domain): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL,'domain', $domain),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
