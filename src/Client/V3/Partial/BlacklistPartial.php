<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:49
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\BlacklistPartialInterface;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Struct\BlacklistedEmail;
use ADevTeam\CleverReachBundle\Struct\ReceiverValidationStruct;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class BlacklistPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class BlacklistPartial extends AbstractPartial implements BlacklistPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/blacklist';

    /**
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $email
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOneByMail(string $email): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $email),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param BlacklistedEmail $blacklistedEmail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function add(BlacklistedEmail $blacklistedEmail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => $blacklistedEmail->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param ReceiverValidationStruct $struct
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getNotBlacklistedByGivenMails(ReceiverValidationStruct $struct): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, 'validate'),
                [
                    'form_params' => $struct->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param BlacklistedEmail $blacklistedEmail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function update(BlacklistedEmail $blacklistedEmail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => $blacklistedEmail->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $email
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $email): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $email),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
