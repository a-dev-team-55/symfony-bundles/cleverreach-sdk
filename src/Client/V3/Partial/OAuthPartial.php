<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:25
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\OAuthPartialInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class OAuthPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class OAuthPartial extends AbstractPartial implements OAuthPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/oauth';

    /**
     * delete mycontent item
     *
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
