<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:50
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\ContentPartialInterface;
use ADevTeam\CleverReachBundle\Struct\ContentItemStruct;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class ContentPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class ContentPartial extends AbstractPartial implements ContentPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/mycontent';

    /**
     * gets a list of all "my content" items
     *
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * gets a certain "my content" item by id
     *
     * @param string $itemId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOneByItemId(string $itemId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $itemId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * adds a new "my content" item
     *
     * @param ContentItemStruct $item
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function add(ContentItemStruct $item): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => $item->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * modifies an existing "my content" item
     *
     * @param string $itemId
     * @param ContentItemStruct $item
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateByItemId(string $itemId, ContentItemStruct $item): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $itemId),
                [
                    'form_params' => $item->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * delete mycontent item
     *
     * @param string $itemId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $itemId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $itemId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
