<?php
/*
 * @author ADevTeam
 * @created 15.11.2023 18:24
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

declare(strict_types=1);

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\FormPartialInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class FormPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class FormPartial extends AbstractPartial implements FormPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/forms';

    /**
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $formId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOneByFormId(string $formId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $formId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return code for embedding
     *
     * Note:
     * This Explorer unescapes the return values.
     * Therefore, it unescapes HTML from this endpoint!
     * Your browser tries to display it, resulting in no result and seemingly endless loading.
     * Please use this endpoint in your browser directly to try it.
     *
     * @param string $formId
     * @param bool $badget      // Enable Badget (Disable only for non free plans) (default: false).
     * @param bool $embedded    // Embedded (default: false).
     * @param bool $js          // Embedded JS (default: true).
     * @param bool $css         // Embedded CSS (default: true).
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getEmbeddingCode(
        string $formId,
        bool $badget = false,
        bool $embedded = false,
        bool $js = true,
        bool $css = true
    ): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $formId, 'code'),
                [
                    'query' => [
                        'badget' => $badget,
                        'embedded' => $embedded,
                        'js' => $js,
                        'css' => $css
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $formId
     * @param string $email
     * @param array|null $doidata
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function sendSubscribeMail(string $formId, string $email, ?array $doidata = null): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $formId, 'send', 'activate'),
                [
                    'form_params' => [
                        'email' => $email,
                        'doidata' => $doidata ?? [
                                'user_ip' => long2ip(rand(0, 4294967295)),
                                'referer' => '',
                                'user_agent' => $this->getRandomUserAgent(),
                            ]
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $formId
     * @param string $email
     * @param array|null $doidata
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function sendUnsubscribeMail(string $formId, string $email, ?array $doidata = null): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $formId, 'send', 'deactivate'),
                [
                    'form_params' => [
                        'email' => $email,
                        'doidata' => $doidata ?? [
                                'user_ip' => long2ip(rand(0, 4294967295)),
                                'referer' => '',
                                'user_agent' => $this->getRandomUserAgent(),
                            ]
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Creates a form from a template by given type
     *
     * Example Post Data (extradata for type 'default'):
     * {
     *   "name": "Form for Batnews",
     *   "title": "Batnews"
     * }
     *
     * You should specify a name and a title.
     * If no name is given, the title is taken.
     * 400 if you don't specify anything.
     *
     * @param string $groupId
     * @param string $type
     * @param array $extradata
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function createFromTemplate(string $groupId, string $type = 'default', array $extradata = []): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'createfromtemplate', $type),
                [
                    'form_params' => $extradata,
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $formId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $formId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $formId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
