<?php
/*
 * @author ADevTeam
 * @created 15.11.2023 19:06
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

/** @noinspection PhpDuplicateCatchBodyInspection */
/** @noinspection DuplicatedCode */
declare(strict_types=1);

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Constant\OrderDirection;
use ADevTeam\CleverReachBundle\Http\CleverReachCountResponse;
use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\CleverReachCountResponseInterface;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Interface\GroupPartialInterface;
use ADevTeam\CleverReachBundle\Struct\AttributeStruct;
use ADevTeam\CleverReachBundle\Struct\EventStruct;
use ADevTeam\CleverReachBundle\Struct\FilterStruct;
use ADevTeam\CleverReachBundle\Struct\OrderStruct;
use ADevTeam\CleverReachBundle\Struct\ReceiverList;
use ADevTeam\CleverReachBundle\Struct\ReceiverPagination;
use ADevTeam\CleverReachBundle\Struct\GroupStruct;
use ADevTeam\CleverReachBundle\Struct\UpdateAttributeStruct;
use Fiber;
use ADevTeam\CleverReachBundle\Struct\ReceiverStruct;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Throwable;

/**
 * @class GroupPartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class GroupPartial extends AbstractPartial implements GroupPartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/groups';

    /**
     * returns array of groups (receiver lists)
     *
     * @param OrderDirection $order
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(OrderDirection $order = OrderDirection::DESC): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                [
                    'query' => [
                        'order' => $order->value,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return extended stats about the group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getExtendedStats(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'advancedstats'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return local attributes.
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getGroupAttributes(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'attributes'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * returns a list with all blacklisted email addresse for a group
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getBlacklist(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'blacklist'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return filters
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getFilters(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'filters'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return groups statistics based on a Filter
     *
     * @param string $groupId
     * @param string $filterId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getCountStatisticBasedOnFilter(string $groupId, string $filterId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'filters', $filterId, 'count'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return a list of receivers
     *
     * @param string $groupId
     * @param string $filterId
     * @param ReceiverPagination $receiverPagination
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceiversBasedOnFilter(string $groupId, string $filterId, ReceiverPagination $receiverPagination): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'filters', $filterId, 'receivers'),
                [
                    'query' => $receiverPagination->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $groupId
     * @param int|null $batchSize
     * @return array
     * @throws Throwable
     */
    public function getAllReceiversByGroupId(string $groupId, int $batchSize = null): array
    {
        if (is_null($batchSize)) {
            $batchSize = $this->batchSize;
        }

        $totalCount = $this->getTotalCountByGroupId($groupId)->getValue();

        $receivers = [];
        $fibers = [];

        for ($i = 1; $i <= ceil($totalCount / $batchSize); $i++) {
            $currentBatchSize = $i * $batchSize < $totalCount ? $batchSize : $totalCount - ($i - 1) * $batchSize;

            $pagination = new ReceiverPagination();
            $pagination
                ->setPage($i - 1)
                ->setPageSize($currentBatchSize);

            $fibers[] = new Fiber(function () use($groupId, $pagination) {
                Fiber::suspend($this->getReceivers($groupId, $pagination)->toArray());
            });
        }

        /** @var Fiber $fiber */
        foreach ($fibers as $fiber) {
            $receivers = array_merge($receivers, $fiber->start());
        }

        return $receivers;
    }

    /**
     * return groups statistics based on a Filter
     *
     * @param string $groupId
     * @param string $filterId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getStatisticBasedOnFilter(string $groupId, string $filterId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'filters', $filterId, 'stats'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return a list of receivers
     *
     * @param string $groupId
     * @param ReceiverPagination $receiverPagination
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceivers(string $groupId, ReceiverPagination $receiverPagination): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'receivers'),
                [
                    'query' => $receiverPagination->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return specific receiver of a certain group
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return a list of events for a receiver
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceiverEvents(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'events'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return a list of orders
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getReceiverOrders(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'orders'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return group by given id
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOne(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return group forms
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getForms(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'forms'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * return stats about the group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getStats(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL, $groupId, 'stats'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * create a list of receivers
     *
     * Example Post Data:
     * {
     *   "name": "Justice League",
     *   "receiver_info": "Superhero colleagues",
     *   "locked": false,
     *   "backup": true
     * }
     *
     * @param GroupStruct $group
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function create(GroupStruct $group): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => $group->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * create attribute
     *
     * Example Post Data:
     * {
     *   "name": "secret_id",
     *   "type": "text",
     *   "description": "Secret identity",
     *   "preview_value": "real name",
     *   "default_value": "Bruce Wayne"
     * }
     *
     * @param string $groupId
     * @param AttributeStruct $attribute
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function createAttribute(string $groupId, AttributeStruct $attribute): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'attributes'),
                [
                    'form_params' => $attribute->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * add an email address to group blacklist
     *
     * @param string $groupId
     * @param string $email
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function addMailToGroupBlacklist(string $groupId, string $email): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'blacklist'),
                [
                    'form_params' => [
                        'email' => $email
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * creates a filter/segment
     *
     * Example Post Data:
     * {
     *   "name": "My Filter",
     *   "operator": "OR",
     *   "rules": [
     *     { "field":"email", "logic":"contains", "condition":"@gotham.com" },
     *     { "field":"activated", "logic":"bg", "condition":"1234567" },
     *     { "field":"firstname", "logic":"eq", "condition":"bruce" },
     *     { "field":"lastname", "logic":"eq", "condition":"wayne" }
     *   ]
     * }
     *
     * Possible Operators
     * OR
     * AND (default)
     *
     * Available Logics:
     * eq - equals
     * neq - not equals
     * sm - smaller
     * bg - bigger
     * isnull - is empty
     * notisnull - not empty
     * contains - contains given string
     * begins - begins with
     * ends - ends with
     * ismale - given field is male
     * isfemale - given field is female
     *
     * general available fields:
     * email - (email of the receiver)
     * source - (import source of the email)
     * imported - (timestamp of receiver import)
     * registered - (timestamp of receiver registration)
     * activated - (timestamp of receiver activation)
     * deactivated - (timestamp of receiver deactivation)
     * bounced - (timestamp of receiver bounce)
     * points - (points of the receiver)
     * last_location - (last location of receiver)
     * last_client - (last mail client used by receiver)
     * crevent_mail_send (logic: eq, condition: mailing_id -> receiver of certain mailing)
     * crevent_mail_open (logic: eq, condition: mailing_id -> opened mail of certain mailing)
     * crevent_mail_click (logic: eq, condition: mailing_id -> clicked on mail of certain mailing)
     * crevent_mail_bounce (logic: eq, condition: mailing_id -> bounced mail of certain mailing)
     * all attributes (if global and group attributes have the same name, group ones will be favoured)
     *
     * @param string $groupId
     * @param FilterStruct $filter
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function createFilter(string $groupId, FilterStruct $filter): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'filters'),
                [
                    'form_params' => $filter->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * create a new receiver
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * {
     *   "email":"bruce@gotham.com",
     *   "registered": "1118354400",
     *   "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *   "deactivated": "0", // omit field unless you know what you are doing!
     *   "source":"Batcave Computer",
     *   "attributes": {
     *     "is_batman":"1"
     *   },
     *   "global_attributes": {
     *     "firstname":"Bruce",
     *     "lastname":"Wayne"
     *   },
     *   "tags": [ "hero", "rich", "smart" ],
     *   "orders": [
     *     {
     *       "order_id": "xyz123412",
     *       "product_id": "SN9876543",
     *       "product": "Batman - The Movie (DVD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     },
     *     {
     *       "order_id": "xyz123411",
     *       "product_id": "SN9876542",
     *       "product": "Batman - The Soundtrack (CD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     }
     *   ]
     * }
     *
     * @param string $groupId
     * @param ReceiverStruct $receiver
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function createReceiver(string $groupId, ReceiverStruct $receiver): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers'),
                [
                    'form_params' => $receiver->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Delete receivers by given ids or emails.
     *
     * Example Post Data:
     * [
     *   "381940",
     *   "dick.grayson@wayne.com",
     *   "robin@gotham.com",
     *   "3571983"
     * ]
     *
     * @param string $groupId
     * @param ReceiverList $receivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deleteMultipleReceivers(string $groupId, ReceiverList $receivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', 'delete'),
                [
                    'form_params' => $receivers->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * create multiple receivers
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "registered": "1118354400",
     *     "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *     "deactivated": "0", // omit field unless you know what you are doing!
     *     "source":"Batcave Computer",
     *     "attributes": {
     *       "is_batman":"1"
     *     },
     *     "global_attributes": {
     *       "firstname":"Bruce",
     *       "lastname":"Wayne"
     *     },
     *     "tags": [ "hero", "rich", "smart" ],
     *     "orders": [
     *       {
     *         "order_id": "xyz123412",
     *         "product_id": "SN9876543",
     *         "product": "Batman - The Movie (DVD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       },
     *       {
     *         "order_id": "xyz123411",
     *         "product_id": "SN9876542",
     *         "product": "Batman - The Soundtrack (CD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       }
     *     ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function createMultipleReceivers(string $groupId, array $receivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', 'insert'),
                [
                    'form_params' => array_map(fn($r) => $r->toArray(), $receivers),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Updates receivers or creates new ones, if email not existing
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "registered": "1118354400",
     *     "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *     "deactivated": "0", // omit field unless you know what you are doing!
     *     "source":"Batcave Computer",
     *     "attributes": {
     *       "is_batman":"1"
     *     },
     *     "global_attributes": {
     *       "firstname":"Bruce",
     *       "lastname":"Wayne"
     *     },
     *     "tags": [ "hero", "rich", "smart" ],
     *     "orders": [
     *       {
     *         "order_id": "xyz123412",
     *         "product_id": "SN9876543",
     *         "product": "Batman - The Movie (DVD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       },
     *       {
     *         "order_id": "xyz123411",
     *         "product_id": "SN9876542",
     *         "product": "Batman - The Soundtrack (CD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       }
     *     ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function upsertMultipleReceivers(string $groupId, array $receivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', 'upsert'),
                [
                    'form_params' => array_map(fn($r) => $r->toArray(), $receivers),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * This is just like upsert, except you can increase or decrease numeric attribute values.
     * Prepend a "+" or "-" to the value to increase or decrease a value.
     *
     * You also can manipulate TAGS in a similar way:
     * In the array of tags, prepend a "-" to the tag you want to be removed.
     * To remove all tags with a specific origin, simply specify "*" instead of any tag name.
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0 and your receiver will definitely be inactive. 0 will eventually reactivate receivers!
     * Just omit 'deactivated' unless you know, what you are doing!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "attributes": {
     *       "weapons":"-4"
     *     },
     *     "global_attributes": {
     *       "title":"winner",
     *       "fights":"+1",
     *       "enemies":"-1"
     *     },
     *     "tags": [ "winner", "-loser", "-joker.*" ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function upsertplusMultipleReceivers(string $groupId, array $receivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', 'upsertplus'),
                [
                    'form_params' => array_map(fn($r) => $r->toArray(), $receivers),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Adds an event to a reveiver
     *
     * Example Post Data:
     * {
     *   "type": "log",
     *   "value": "Entered the bat cave",
     *   "type_id": "1939",
     *   "mailing_id": "28"
     * }
     *
     * @param string $groupId
     * @param array $receiverIdOrMail
     * @param EventStruct $event
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function addReceiverEvent(string $groupId, array $receiverIdOrMail, EventStruct $event): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'events'),
                [
                    'form_params' => $event->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * adds orderitem
     *
     * Example Post Data:
     * {
     *   "order_id": "19891990",
     *   "product": "Batman: The Video Game",
     *   "product_id": "jnr-1989-1990",
     *   "price": 1.99,
     *   "quantity": 2,
     *   "source": "fanshop-gotham",
     *   "currency": "USD"
     * }
     *
     * @param string $groupId
     * @param array $receiverIdOrMail
     * @param OrderStruct $order
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function addOrder(string $groupId, array $receiverIdOrMail, OrderStruct $order): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'orders'),
                [
                    'form_params' => $order->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * update attribute
     *
     * @param string $groupId
     * @param string $attributeId
     * @param UpdateAttributeStruct $attribute
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateAttribute(string $groupId, string $attributeId, UpdateAttributeStruct $attribute): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $groupId, 'attributes', $attributeId),
                [
                    'form_params' => $attribute->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * update attribute
     *
     *  Example Post Data:
     * {
     *   "name": "My Filter",
     *   "operator": "OR",
     *   "rules": [
     *     { "field":"email", "logic":"contains", "condition":"@gotham.com" },
     *     { "field":"activated", "logic":"bg", "condition":"1234567" },
     *     { "field":"firstname", "logic":"eq", "condition":"bruce" },
     *     { "field":"lastname", "logic":"eq", "condition":"wayne" }
     *   ]
     * }
     *
     * Possible Operators
     * OR
     * AND (default)
     *
     * Available Logics:
     * eq - equals
     * neq - not equals
     * sm - smaller
     * bg - bigger
     * isnull - is empty
     * notisnull - not empty
     * contains - contains given string
     * begins - begins with
     * ends - ends with
     * ismale - given field is male
     * isfemale - given field is female
     *
     * general available fields:
     * email - (email of the receiver)
     * source - (import source of the email)
     * imported - (timestamp of receiver import)
     * registered - (timestamp of receiver registration)
     * activated - (timestamp of receiver activation)
     * deactivated - (timestamp of receiver deactivation)
     * bounced - (timestamp of receiver bounce)
     * points - (points of the receiver)
     * last_location - (last location of receiver)
     * last_client - (last mail client used by receiver)
     * crevent_mail_send (logic: eq, condition: mailing_id -> receiver of certain mailing)
     * crevent_mail_open (logic: eq, condition: mailing_id -> opened mail of certain mailing)
     * crevent_mail_click (logic: eq, condition: mailing_id -> clicked on mail of certain mailing)
     * crevent_mail_bounce (logic: eq, condition: mailing_id -> bounced mail of certain mailing)
     * all attributes (if global and group attributes have the same name, group ones will be favoured)
     *
     * @param string $groupId
     * @param string $filterId
     * @param FilterStruct $filter
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateFilter(string $groupId, string $filterId, FilterStruct $filter): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $groupId, 'filters', $filterId),
                [
                    'form_params' => $filter->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Updates receivers if email existing
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "registered": "1118354400",
     *     "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *     "deactivated": "0", // omit field unless you know what you are doing!
     *     "source":"Batcave Computer",
     *     "attributes": {
     *       "is_batman":"1"
     *     },
     *     "global_attributes": {
     *       "firstname":"Bruce",
     *       "lastname":"Wayne"
     *     },
     *     "tags": [ "hero", "rich", "smart" ],
     *     "orders": [
     *       {
     *         "order_id": "xyz123412",
     *         "product_id": "SN9876543",
     *         "product": "Batman - The Movie (DVD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       },
     *       {
     *         "order_id": "xyz123411",
     *         "product_id": "SN9876542",
     *         "product": "Batman - The Soundtrack (CD)",
     *         "price": 9.99,
     *         "currency": "EUR",
     *         "amount": 1,
     *         "mailing_id": "12345678"
     *       }
     *     ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateReceivers(string $groupId, array $receivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', 'update'),
                [
                    'form_params' => array_map(fn($r) => $r->toArray(), $receivers),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * This is just like upsert, except you can increase or decrease numeric attribute values.
     * Prepend a "+" or "-" to the value to increase or decrease a value.
     *
     * You also can manipulate TAGS in a similar way:
     * In the array of tags, prepend a "-" to the tag you want to be removed.
     * To remove all tags with a specific origin, simply specify "*" instead of any tag name.
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0 and your receiver will definitely be inactive. 0 will eventually reactivate receivers!
     * Just omit 'deactivated' unless you know, what you are doing!
     *
     * Example Post Data:
     * [
     *   {
     *     "email":"bruce@gotham.com",
     *     "attributes": {
     *       "weapons":"-4"
     *     },
     *     "global_attributes": {
     *       "title":"winner",
     *       "fights":"+1",
     *       "enemies":"-1"
     *     },
     *     "tags": [ "winner", "-loser", "-joker.*" ]
     *   }
     * ]
     *
     * @param string $groupId
     * @param ReceiverStruct[] $receivers
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updatePlusReceivers(string $groupId, array $receivers): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', 'updateplus'),
                [
                    'form_params' => array_map(fn($r) => $r->toArray(), $receivers),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * Update any information of a receiver
     *
     * Carefully use the fields 'registered', 'activated' and 'deactivated':
     *
     * - provide 'registered' with current time if receiver is new.
     * - provide 'activated' only for immedite activation (no DOI mail will work then).
     * - provide 'deactivated' other than 0, and your receiver will definitely be inactive.
     *
     * Omission of all of those fields leads to an activated receiver ('registered' and 'activated' to current time)!
     *
     * Example Post Data:
     * {
     *   "email":"bruce@gotham.com",
     *   "registered": "1118354400",
     *   "activated": "1118354400", // omit if receiver is to be activated later, e.g. DOI mail
     *   "deactivated": "0", // omit field unless you know what you are doing!
     *   "source":"Batcave Computer",
     *   "attributes": {
     *     "is_batman":"1"
     *   },
     *   "global_attributes": {
     *     "firstname":"Bruce",
     *     "lastname":"Wayne"
     *   },
     *   "tags": [ "hero", "rich", "smart" ],
     *   "orders": [
     *     {
     *       "order_id": "xyz123412",
     *       "product_id": "SN9876543",
     *       "product": "Batman - The Movie (DVD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     },
     *     {
     *       "order_id": "xyz123411",
     *       "product_id": "SN9876542",
     *       "product": "Batman - The Soundtrack (CD)",
     *       "price": 9.99,
     *       "currency": "EUR",
     *       "amount": 1,
     *       "mailing_id": "12345678"
     *     }
     *   ]
     * }
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @param ReceiverStruct $receiver
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateReceiver(string $groupId, string $receiverIdOrMail, ReceiverStruct $receiver): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail),
                [
                    'form_params' => $receiver->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function activateReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'activate'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deactivateReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'deactivate'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * update value of an attribute of a receiver
     *
     * @param string $groupId
     * @param string $attributeId
     * @param string $receiverIdOrMail
     * @param UpdateAttributeStruct $attribute
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateAttributeValue(string $groupId, string $attributeId, string $receiverIdOrMail, UpdateAttributeStruct $attribute): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'attributes', $attributeId),
                [
                    'form_params' => $attribute->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * updates orderitem
     *
     * Example Post Data:
     * {
     *   "order_id": "19891990",
     *   "product": "Batman: The Video Game",
     *   "product_id": "jnr-1989-1990",
     *   "price": 1.99,
     *   "quantity": 2,
     *   "source": "fanshop-gotham",
     *   "currency": "USD"
     * }
     *
     * @param string $groupId
     * @param string $orderId
     * @param string $receiverIdOrMail
     * @param OrderStruct $order
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function updateOrder(string $groupId, string $orderId, string $receiverIdOrMail, OrderStruct $order): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'orders', $orderId),
                [
                    'form_params' => $order->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * update a list of receivers
     *
     * Example Post Data:
     * {
     *   "name": "Justice League",
     *   "receiver_info": "Superhero colleagues",
     *   "locked": false,
     *   "backup": true
     * }
     *
     * @param string $groupId
     * @param GroupStruct $group
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function update(string $groupId, GroupStruct $group): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL, $groupId),
                [
                    'form_params' => $group->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * delete attribute
     *
     * @param string $groupId
     * @param string $attributeId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deleteAttribute(string $groupId, string $attributeId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $groupId, 'attributes', $attributeId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * delete attribute
     *
     * @param string $groupId
     * @param string $filterId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deleteFilter(string $groupId, string $filterId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $groupId, 'filters', $filterId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * delete receiver
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deleteReceiver(string $groupId, string $receiverIdOrMail): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * delete order
     *
     * @param string $groupId
     * @param string $receiverIdOrMail
     * @param string $orderId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function deleteOrder(string $groupId, string $receiverIdOrMail, string $orderId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $groupId, 'receivers', $receiverIdOrMail, 'orders', $orderId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * delete group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $groupId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * removes email address from group blacklist
     *
     * @param string $groupId
     * @param string $email
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function removeBlacklistedMail(string $groupId, string $email): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $groupId, 'blacklist', $email),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * delete all email addresses from group (receiver list)
     *
     * @param string $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function truncate(string $groupId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $groupId, 'clear'),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     * @throws GuzzleException
     */
    public function getTotalCountByGroupId(string $groupId): CleverReachCountResponseInterface
    {
        return new CleverReachCountResponse($this->getStats($groupId), 'total_count');
    }

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     * @throws GuzzleException
     */
    public function getActiveCountByGroupId(string $groupId): CleverReachCountResponseInterface
    {
        return new CleverReachCountResponse($this->getStats($groupId), 'active_count');
    }

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     * @throws GuzzleException
     */
    public function getInactiveCountByGroupId(string $groupId): CleverReachCountResponseInterface
    {
        return new CleverReachCountResponse($this->getStats($groupId), 'inactive_count');
    }

    /**
     * @param string $groupId
     * @return CleverReachCountResponseInterface
     * @throws GuzzleException
     */
    public function getBounceCountByGroupId(string $groupId): CleverReachCountResponseInterface
    {
        return new CleverReachCountResponse($this->getStats($groupId), 'bounce_count');
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
