<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:50
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Partial;

use ADevTeam\CleverReachBundle\Http\CleverReachResponse;
use ADevTeam\CleverReachBundle\Interface\AttributePartialInterface;
use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use ADevTeam\CleverReachBundle\Struct\AttributeStruct;
use ADevTeam\CleverReachBundle\Struct\UpdateAttributeStruct;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * @class AttributePartial
 * @package ADevTeam\CleverReachBundle\Client\V3\Partial
 * @extends AbstractPartial
 */
class AttributePartial extends AbstractPartial implements AttributePartialInterface
{

    /**
     * @var string
     */
    public const BASE_URL = parent::BASE_URL_V3 . '/attributes';

    /**
     * @param string|null $groupId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getAll(?string $groupId = null): CleverReachResponseInterface
    {
        $options = ['on_stats' => $this->getStatHandler($request)];

        if ($groupId) {
            $options['form_params'] = ['group_id' => $groupId];
        }

        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                $options
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $attributeId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function getOne(string $attributeId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->get(
                self::buildUri(self::BASE_URL),
                [
                    'query' => [
                        'id' => $attributeId,
                    ],
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param AttributeStruct $attribute
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function create(AttributeStruct $attribute): CleverReachResponseInterface
    {
        try {
            $response = $this->client->post(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => $attribute->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param UpdateAttributeStruct $attribute
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function update(UpdateAttributeStruct $attribute): CleverReachResponseInterface
    {
        try {
            $response = $this->client->put(
                self::buildUri(self::BASE_URL),
                [
                    'form_params' => $attribute->toArray(),
                    'on_stats' => $this->getStatHandler($request)
                ]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @param string $attributeId
     * @return CleverReachResponseInterface
     * @throws GuzzleException
     */
    public function delete(string $attributeId): CleverReachResponseInterface
    {
        try {
            $response = $this->client->delete(
                self::buildUri(self::BASE_URL, $attributeId),
                ['on_stats' => $this->getStatHandler($request)]
            );

            return new CleverReachResponse($response, $request, $this->requestLogger, $this->responseLogger);
        } catch (RequestException $e) {
            return new CleverReachResponse(
                $e->getResponse(),
                $e->getRequest(),
                $this->requestLogger,
                $this->responseLogger
            );
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::BASE_URL;
    }
}
