<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 22:40
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Command;

use ADevTeam\CleverReachBundle\Command\AbstractCommand;

/**
 * @class AbstractV3Command
 * @package ADevTeam\CleverReachBundle\Client\V3\Command
 */
abstract class AbstractV3Command extends AbstractCommand
{

    public const API_VERSION = 3;

    public const COMMAND_PREFIX = parent::COMMAND_PREFIX . ':v' . self::API_VERSION . ':';
    public const DECSCRIPTION_PREFIX = parent::DECSCRIPTION_PREFIX . ' V' . self::API_VERSION . ' - ';
}
