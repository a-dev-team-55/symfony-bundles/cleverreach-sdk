<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 17.11.2023 21:41
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Command\Receiver;

use ADevTeam\CleverReachBundle\Client\V3\Command\AbstractV3Command;
use ADevTeam\CleverReachBundle\Command\AbstractCommand;
use ADevTeam\CleverReachBundle\Constant\TableHeader;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @class GetOneCommand
 * @package ADevTeam\CleverReachBundle\Client\V3\Command\Receiver
 * @extends AbstractCommand
 */
#[AsCommand(
    name: AbstractV3Command::COMMAND_PREFIX . 'receiver:get-one',
    description: AbstractV3Command::DECSCRIPTION_PREFIX . 'Get one receiver by groupID and receiverId order receiverMail'
)]
class GetOneCommand extends AbstractV3Command
{

    /**
     * @return void
     */
    protected function configure(): void
    {
        parent::configure();

        $this
            ->addArgument(
                'group-id',
                InputArgument::REQUIRED,
                'Clever Reach - Group ID'
            )
            ->addArgument(
                'receiver-id',
                InputArgument::REQUIRED,
                'Clever Reach - Receiver ID or Mail'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        $groupId = $input->getArgument('group-id');
        $receiverId = $input->getArgument('receiver-id');

        $receiver = $this->client->receivers()->getOne($groupId, $receiverId);
        $globalAttributes = $receiver['global_attributes'] ?? [];
        $events = $receiver['events'] ?? [];

        $this->renderTable(TableHeader::RECEIVER, [$receiver], 'Receiver');
        $this->renderTable(TableHeader::EVENT, $events, 'Events');
        $this->renderTable(array_keys($globalAttributes), array_values($globalAttributes), 'Global Attributes');


        return Command::SUCCESS;
    }
}
