<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 18:39
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Command\Group;

use ADevTeam\CleverReachBundle\Client\V3\Command\AbstractV3Command;
use ADevTeam\CleverReachBundle\Command\AbstractCommand;
use ADevTeam\CleverReachBundle\Constant\TableHeader;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @class GetStatsCommand
 * @package ADevTeam\CleverReachBundle\Client\V3\Command\Group
 * @extends AbstractCommand
 */
#[AsCommand(
    name: AbstractV3Command::COMMAND_PREFIX . 'group:get-stats',
    description: AbstractV3Command::DECSCRIPTION_PREFIX . 'Get group Statistics by group ID'
)]
class GetStatsCommand extends AbstractV3Command
{

    /**
     * @return void
     */
    protected function configure(): void
    {
        parent::configure();

        $this
            ->addArgument(
                'group-id',
                InputArgument::REQUIRED,
                'Clever Reach - Group ID'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        $groupId = $input->getArgument('group-id');

        $stats = $this->client->groups()->getStats($groupId)->toArray();

        $this->io->table(TableHeader::STATS, [$stats]);

        return Command::SUCCESS;
    }
}
