<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 18:39
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Command\Group;

use ADevTeam\CleverReachBundle\Client\V3\Command\AbstractV3Command;
use ADevTeam\CleverReachBundle\Command\AbstractCommand;
use ADevTeam\CleverReachBundle\Constant\TableHeader;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

/**
 * @class GetReceiversCommand
 * @package ADevTeam\CleverReachBundle\Client\V3\Command\Group
 * @extends AbstractCommand
 */
#[AsCommand(
    name: AbstractV3Command::COMMAND_PREFIX . 'group:get-receivers',
    description: AbstractV3Command::DECSCRIPTION_PREFIX . 'Get Group receivers by group ID'
)]
class GetReceiversCommand extends AbstractV3Command
{

    /**
     * @return void
     */
    protected function configure(): void
    {
        parent::configure();

        $this
            ->addArgument(
                'group-id',
                InputArgument::REQUIRED,
                'Clever Reach - Group ID'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        $groupId = $input->getArgument('group-id');

        $receivers = $this->client->groups()->getAllReceiversByGroupId($groupId);

        foreach ($receivers as &$receiver) {
            unset($receiver['attributes'], $receiver['global_attributes']);
        }

        $this->io->table(TableHeader::RECEIVER, $receivers);

        return Command::SUCCESS;
    }
}
