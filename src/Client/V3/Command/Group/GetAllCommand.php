<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 18:39
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Command\Group;

use ADevTeam\CleverReachBundle\Client\V3\Command\AbstractV3Command;
use ADevTeam\CleverReachBundle\Command\AbstractCommand;
use ADevTeam\CleverReachBundle\Constant\TableHeader;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @class GetAllCommand
 * @package ADevTeam\CleverReachBundle\Client\V3\Command\Group
 * @extends AbstractCommand
 */
#[AsCommand(
    name: AbstractV3Command::COMMAND_PREFIX . 'group:get-all',
    description: AbstractV3Command::DECSCRIPTION_PREFIX . 'Get all Groups'
)]
class GetAllCommand extends AbstractV3Command
{

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        $groups = $this->client->groups()->getAll();

        $this->io->table(TableHeader::GROUP, $groups->toArray());

        return Command::SUCCESS;
    }
}
