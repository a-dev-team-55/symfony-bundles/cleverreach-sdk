<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 18:39
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Client\V3\Command\Group;

use ADevTeam\CleverReachBundle\Client\V3\Command\AbstractV3Command;
use ADevTeam\CleverReachBundle\Command\AbstractCommand;
use ADevTeam\CleverReachBundle\Struct\ReceiverStruct;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @class GetStatsCommand
 * @package ADevTeam\CleverReachBundle\Client\V3\Command\Group
 * @extends AbstractCommand
 */
#[AsCommand(
    name: AbstractV3Command::COMMAND_PREFIX . 'group:add-receiver',
    description: AbstractV3Command::DECSCRIPTION_PREFIX . 'Add group receiver'
)]
class AddReceiverCommand extends AbstractV3Command
{

    /**
     * @return void
     */
    protected function configure(): void
    {
        parent::configure();

        $this
            ->addArgument(
                'group-id',
                InputArgument::REQUIRED,
                'Clever Reach - Group ID'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        $groupId = $input->getArgument('group-id');

//        $email = $this->questions->getEmail();

//        $receiver = new ReceiverStruct($email);
        $receiver = new ReceiverStruct('a.gerhardt-tag-test@gmail.com');
        $receiver->setTags([
            'Admin',
            'Entity',
            'Grüner Tee'
        ]);

//        if ($this->questions->isReceiverRegistered()) {
//            $receiver->setRegistered($this->questions->getReceiverRegistrationDate());
//        }
//
//        if ($this->questions->isReceiverActivated()) {
//            $receiver->setActivated($this->questions->getReceiverActivationDate());
//        }
//
//        if ($this->questions->isReceiverDeactivated()) {
//            $receiver->setDeactivated($this->questions->getReceiverDeactivationDate());
//        }

        $receiver = $this->client->groups()->createReceiver($groupId, $receiver)->toArray();

        foreach ($receiver as $key => $value) {
            if (is_array($value)) {
                unset($receiver[$key]);
            }
        }

        $this->io->info(print_r($receiver, true));

        return Command::SUCCESS;
    }
}
