<?php /** @noinspection PhpMultipleClassDeclarationsInspection */ /*
 * @author ADevTeam
 * @created 15.11.2023 17:49
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */ /** @noinspection PhpDuplicateCatchBodyInspection */
declare(strict_types=1);

namespace ADevTeam\CleverReachBundle\Command;

use ADevTeam\CleverReachBundle\Struct\ReceiverStruct;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @class TestCommand
 * @package ADevTeam\CleverReachBundle\Command
 * @extends Command
 */
#[AsCommand(
    name: 'adt:cr:test',
    description: 'Cleverreach V3 - Test'
)]
class TestCommand extends AbstractCommand
{

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $clientId = $input->getArgument('client-id');
        $clientSecret = $input->getArgument('client-secret');

        $this->client
            ->setClientId($clientId)
            ->setClientSecret($clientSecret)
            ->authenticate();

        dump($this->client->getScopes());

        return Command::SUCCESS;
    }
}
