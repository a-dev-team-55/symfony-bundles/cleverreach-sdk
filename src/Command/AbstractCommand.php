<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 17:58
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Command;

use ADevTeam\CleverReachBundle\Client\V3\CleverReachClient;
use ADevTeam\CleverReachBundle\Constant\TableHeader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @class AbstractCommand
 * @package ADevTeam\CleverReachBundle\Command
 */
abstract class AbstractCommand extends Command
{

    public const COMMAND_PREFIX = 'cr';
    public const DECSCRIPTION_PREFIX = 'CleverReach';

    /**
     * @var SymfonyStyle $io
     */
    protected SymfonyStyle $io;

    /**
     * @var CommandQuestionHelper $questions
     */
    protected CommandQuestionHelper $questions;

    /**
     * @var OutputInterface $output
     */
    private OutputInterface $output;

    /**
     * @param CleverReachClient $client
     */
    public function __construct(protected readonly CleverReachClient $client)
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addArgument(
                'client-id',
                InputArgument::REQUIRED,
                'Clever Reach - Client ID'
            )
            ->addArgument(
                'client-secret',
                InputArgument::REQUIRED,
                'Clever Reach - Client Secret'
            );
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->questions = new CommandQuestionHelper($input, $output);

        $this->output = $output;

        $clientId = $input->getArgument('client-id');
        $clientSecret = $input->getArgument('client-secret');

        $authenticated = $this->client
            ->setClientId($clientId)
            ->setClientSecret($clientSecret)
            ->authenticate();

        if ($authenticated) {
            $this->io->title('Successfully authenticated');
        } else {
            $this->io->title('Authentication failed');
        }

        return Command::SUCCESS;
    }

    /**
     * @param array $headers
     * @param array $data
     * @param string|null $title
     * @return void
     */
    protected function renderTable(array $headers, array $data = [], ?string $title = null): void
    {
        if (!empty($data) && !empty($headers)) {
            $this->io->write(PHP_EOL . PHP_EOL);

            if ($title) {
                $this->io->title($title);
            }

            $data = array_map(fn($i) => array_filter(is_array($i) ? $i : [$i], fn($v) => !is_array($v)), $data);
            $rows = array_map(fn($i) => array_map(fn($v) => new TableCell((string)$v), $i), $data);

            (new Table($this->output))
                ->setHeaders($headers)
                ->setRows($rows)
                ->render();
        }
    }
}
