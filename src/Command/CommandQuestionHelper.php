<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 23:26
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Command;

use DateTime;
use RuntimeException;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * @class CommandQuestions
 * @package ADevTeam\CleverReachBundle\Command
 */
class CommandQuestionHelper
{

    /**
     * @var QuestionHelper $questionHelper
     */
    private QuestionHelper $helper;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function __construct(
        private InputInterface  $input,
        private OutputInterface $output
    ) {
        $this->helper = new QuestionHelper();
    }


    /**
     * @param string $questionText
     * @return string
     */
    public function getEmail(string $questionText = 'E-Mail:'): string
    {
        $question = new Question('<comment>' . $questionText . '</comment> ');
        $question->setTrimmable(true);
        $question->setValidator(function (?string $answer): ?string {
            if (!filter_var($answer, FILTER_VALIDATE_EMAIL)) {
                throw new RuntimeException(
                    'This is not a valid e-mail address.'
                );
            }

            return $answer;
        });

        return $this->helper->ask($this->input, $this->output, $question);
    }

    /**
     * @param string $questionText
     * @param string $format
     * @param string $separator
     * @return array
     */
    public function getDate(string $questionText, string $format = 'd.m.Y', string $separator = '.'): array
    {
        if (!str_contains($format, 'd') || !str_contains($format, 'm') || !str_contains($format, 'Y')) {
            throw new RuntimeException(
                sprintf('%s is not a valid date format.', $format)
            );
        }

        $question = new Question('<comment>' . $questionText . '</comment> ');
        $question->setTrimmable(true);
        $question->setValidator(function (?string $answer) use($format, $separator): ?array {
            $date = explode($separator, $answer);
            $order = array_flip(explode($separator, $format));

            if (!empty($answer)) {
                $date = array_map(function ($dateKey) use($date, $order) {
                    return $date[$order[$dateKey] ?? 0] ?? date($dateKey);
                }, array_keys($order));

                if (
                    !checkdate((int)$date[$order['m']], (int)$date[$order['d']], (int)$date[$order['Y']]) ||
                    (int)$date[$order['Y']] > (int)date("Y")
                ) {
                    throw new RuntimeException(
                        'This is not a valid date'
                    );
                }
            }

            return [
                'day' => str_pad(trim($date[$order['d'] ?? 0]), 2, '0', STR_PAD_LEFT),
                'month' => str_pad(trim($date[$order['m'] ?? 0]), 2, '0', STR_PAD_LEFT),
                'year' => str_pad(trim($date[$order['Y'] ?? 0]), 4, '0', STR_PAD_LEFT)
            ];
        });

        return $this->helper->ask($this->input, $this->output, $question);
    }

    /**
     * @param string $questionText
     * @param string $format
     * @param string $separator
     * @return array
     */
    public function getTime(string $questionText, string $format = 'H:i:s', string $separator = ':'): array
    {
        if (!str_contains($format, 'H') || !str_contains($format, 'i') || !str_contains($format, 's')) {
            throw new RuntimeException(
                sprintf('%s is not a valid time format.', $format)
            );
        }

        $question = new Question('<comment>' . $questionText . '</comment> ', '12:00:00');
        $question->setTrimmable(false);
        $question->setValidator(function (string $answer = '12:00:00') use($format, $separator): ?array {
            $time = explode($separator, $answer);
            $order = array_flip(explode($separator, $format));

            if (!empty($answer)) {
                if ($time != array_filter($time, 'is_numeric')) {
                    throw new RuntimeException(
                        'This is not a valid time'
                    );
                }

                $time = array_map(function ($timeKey) use($time, $order) {
                    return $time[$order[$timeKey] ?? 0] ?? ($timeKey === 'H' ? '12' : '00');
                }, array_keys($order));

                if (
                    (int)$time[$order['H']] < 0 || (int)$time[$order['H']] > 23 ||
                    (int)$time[$order['i']] < 0 || (int)$time[$order['i']] > 59 ||
                    (int)$time[$order['s']] < 0 || (int)$time[$order['s']] > 59
                ) {
                    throw new RuntimeException(
                        'This is not a valid time'
                    );
                }
            }

            return [
                'hour' => str_pad(trim($time[$order['H'] ?? 0]), 2, '0', STR_PAD_LEFT),
                'minutes' => str_pad(trim($time[$order['i'] ?? 0]), 2, '0', STR_PAD_LEFT),
                'seconds' => str_pad(trim($time[$order['s'] ?? 0]), 2, '0', STR_PAD_LEFT)
            ];
        });

        return $this->helper->ask($this->input, $this->output, $question);
    }

    /**
     * @param string $dateQuestionText
     * @param string $timeQuestionText
     * @param string $dateFormat
     * @param string $timeFormat
     * @param string $dateSeparator
     * @param string $timeSeparator
     * @return DateTime|false
     */
    public function getDateTime(
        string $dateQuestionText,
        string $timeQuestionText,
        string $dateFormat = 'd.m.Y',
        string $timeFormat = 'H:i:s',
        string $dateSeparator = '.',
        string $timeSeparator = ':',
    ): DateTime|false
    {
        $date = $this->getDate($dateQuestionText, $dateFormat, $dateSeparator);
        $time = $this->getTime($timeQuestionText, $timeFormat, $timeSeparator);

        [$d, $m, $y] = [$date['day'], $date['month'], $date['year']];
        [$h, $i, $s] = [$time['hour'], $time['minutes'], $time['seconds']];

        return DateTime::createFromFormat(
            sprintf('%s %s', $dateFormat, $timeFormat),
            sprintf('%s.%s.%s %s:%s:%s', $d, $m, $y, $h, $i, $s)
        );
    }

    /**
     * @param string $dateQuestionText
     * @param string $timeQuestionText
     * @return DateTime|false
     */
    public function getReceiverRegistrationDate(
        string $dateQuestionText = 'Registration Date:',
        string $timeQuestionText = 'Registration Time:'
    ): DateTime|false
    {
        return $this->getDateTime($dateQuestionText, $timeQuestionText);
    }

    /**
     * @param string $dateQuestionText
     * @param string $timeQuestionText
     * @return DateTime|false
     */
    public function getReceiverActivationDate(
        string $dateQuestionText = 'Activation Date:',
        string $timeQuestionText = 'Activation Time:'
    ): DateTime|false
    {
        return $this->getDateTime($dateQuestionText, $timeQuestionText);
    }

    /**
     * @param string $dateQuestionText
     * @param string $timeQuestionText
     * @return DateTime|false
     */
    public function getReceiverDeactivationDate(
        string $dateQuestionText = 'Deactivation Date:',
        string $timeQuestionText = 'Deactivation Time:'
    ): DateTime|false
    {
        return $this->getDateTime($dateQuestionText, $timeQuestionText);
    }

    /**
     * @param string $questionText
     * @return bool
     */
    public function isReceiverRegistered(string $questionText = '<comment>Is the newsletter recipient registered? (y/n)</comment> '): bool
    {
        $question = new ConfirmationQuestion($questionText, false);
        return $this->helper->ask($this->input, $this->output, $question);
    }

    /**
     * @param string $questionText
     * @return bool
     */
    public function isReceiverActivated(string $questionText = '<comment>Is the newsletter recipient activated? (y/n)</comment> '): bool
    {
        $question = new ConfirmationQuestion($questionText, false);
        return $this->helper->ask($this->input, $this->output, $question);
    }

    /**
     * @param string $questionText
     * @return bool
     */
    public function isReceiverDeactivated(string $questionText = '<comment>Is the newsletter recipient deactivated? (y/n)</comment> '): bool
    {
        $question = new ConfirmationQuestion($questionText, false);
        return $this->helper->ask($this->input, $this->output, $question);
    }

}
