<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:28
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class MailingState
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum MailingState: string
{
    case ALL = 'all';
    case FINISHED = 'finished';
    case DRAFT = 'draft';
    case WAITING = 'waitinh';
    case RUNNING = 'running';
    case AUTOMATION = 'automation';
}
