<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:01
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class StatisticMode
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum StatisticMode: string
{
    case FULL = 'full';
    case CLIENTS = 'clients';
    case MEDIACLIENTS = 'mediaclients';
    case COUNTRIES = 'countries';
    case QUALITY = 'quality';
    case FIRSTHOURS = 'firsthours';
    case TRAFFIC = 'traffic';
    case DAILY = 'daily';
    case LINKS = 'links';
    case CONVERSION = 'conversion';
    case CUSTOM = 'custom';
}
