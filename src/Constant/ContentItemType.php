<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:16
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class ContentItemType
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum ContentItemType: string
{
    case DEFAULT = 'default';
    case PRODUCT = 'product';
    case CONTENT = 'content';
}
