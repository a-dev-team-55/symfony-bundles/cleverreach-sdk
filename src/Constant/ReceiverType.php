<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 27.11.2023 00:10
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class ReceiverType
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum ReceiverType: string
{
    case ALL = 'all';
    case ACTIVE = 'active';
    case INACTIVE = 'inactive';
    case BOUNCE = 'bounce';
}
