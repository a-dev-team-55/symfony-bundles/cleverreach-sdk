<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 19:01
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class TableHeader
 * @package ADevTeam\CleverReachBundle\Constant
 */
abstract class TableHeader
{

    public const FORM = [
        'ID',
        'Name',
        'Customer Tables ID'
    ];

    public const GROUP = [
        'ID',
        'Name',
        'Locked',
        'Backup',
        'Receiver info',
        'Stamp',
        'Last mailing',
        'Last changed'
    ];

    public const STATS = [
        'Total count',
        'Inactive count',
        'Active count',
        'Bounce count',
        'Avg points',
        'Quality',
        'Time',
        'Order count'
    ];

    public const RECEIVER = [
        'ID',
        'Email',
        'Imported',
        'Bounced',
        'Group ID',
        'Activated',
        'Registered',
        'Deactivated',
        'Last IP',
        'Last location',
        'Last client',
        'Points',
        'Stars',
        'Source',
        'Active',
        'Conversion rate',
        'Open rate',
        'Click rate'
    ];

    public const EVENT = [
        'Stamp',
        'Type',
        'Type ID',
        'Value',
        'Mailing ID',
        'Group ID'
    ];
}
