<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:08
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class FilterDetailDepth
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum FilterDetailDepth: int
{
    case NONE = 0;
    case EVENTS = 1;
    case ORDERS = 2;
    case TAGS = 4;
}
