<?php
/*
 * @author ADevTeam
 * @created 26.11.2023 22:03
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

enum FilterLogic: string
{
    case EQ = 'eq';                     // Equals
    case NEQ = 'neq';                   // Not Equals
    case SM = 'sm';                     // Smaller
    case BG = 'bg';                     // Bigger
    case IS_NULL = 'isnull';            // Is Empty
    case NOT_IS_NULL = 'notisnull';     // Not Empty
    case CONTAINS = 'contains';         // Contains given String
    case BEGINS = 'begins';             // Begins with
    case ENDS = 'ends';                 // Ends with
    case IS_MALE = 'ismale';            // Given field is male
    case IS_FEMALE = 'isfemale';        // Given field is female
}
