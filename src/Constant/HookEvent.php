<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 17:49
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @enum HttpMethod
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum HookEvent: string
{

    case RECEIVER_SUBSCRIBED = 'receiver.subscribed';
    case RECEIVER_UNSUBSCRIBED = 'receiver.unsubscribed';
    case RECEIVER_CREATED = 'receiver.created';
    case RECEIVER_UPDATED = 'receiver.updated';
    case RECEIVER_DELETED = 'receiver.deleted';

    case FORM_CREATED = 'form.created';
    case FORM_UPDATED = 'form.updated';
    case FORM_DELETED = 'form.deleted';

    case FILTER_CREATED = 'filter.created';
    case FILTER_UPDATED = 'filter.updated';
    case FILTER_DELETED = 'filter.deleted';

    case GROUP_CREATED = 'group.created';
    case GROUP_UPDATED = 'group.updated';
    case GROUP_DELETED = 'group.deleted';

    case AUTOMATION_ACTIVATED = 'automation.activated';
    case AUTOMATION_DEACTIVATED = 'automation.deactivated';
    case AUTOMATION_DELETED = 'automation.deleted';
}
