<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:46
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class MailingContentType
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum MailingContentType: string
{
    case TEXT = 'text';
    case HTML = 'html';
    case BOTH = 'html/text';
}
