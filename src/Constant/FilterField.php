<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:24
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class FilterField
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum FilterField: string
{
    case EMAIL = 'email';
    case SOURCE = 'source';
    case IMPORTED = 'imported';
    case REGISTERED = 'registered';
    case ACTIVATED = 'activated';
    case DEACTIVATED = 'deactivated';
    case BOUNCED = 'bounced';
    case POINTS = 'points';
    case LAST_LOCATION = 'last_location';
    case LAST_CLIENT = 'last_client';
    case CR_EVENT_MAIL_SEND = 'crevent_mail_send';
    case CR_EVENT_MAIL_OPEN = 'crevent_mail_open';
    case CR_EVENT_MAIL_CLICK = 'crevent_mail_click';
    case CR_EVENT_MAIL_BOUNCE = 'crevent_mail_bounce';
}
