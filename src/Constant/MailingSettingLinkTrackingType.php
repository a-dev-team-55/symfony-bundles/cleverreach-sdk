<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:53
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class MailingSettingLinkTrackingType
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum MailingSettingLinkTrackingType: string
{
    case GOOGLE = 'google';
    case INTELLIAD = 'intelliad';
    case CRCONNECT = 'crconnect';
}
