<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 27.11.2023 23:05
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class ApiScope
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum ApiScope: string
{

    case BASIC = 'BASIC';
    case RECEIVERS = 'receivers';
    case REPORTS = 'reports';
    case FORMS = 'forms';
    case MAILINGS = 'mailings';
    case SSL = 'ssl';
    case WEBHOOKS = 'webhooks';
}
