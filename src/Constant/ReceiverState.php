<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 21:31
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class ReceiverState
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum ReceiverState: string
{
    case ALL = 'all';
    case ACTIVE = 'active';
    case INACTIVE = 'inactive';
}
