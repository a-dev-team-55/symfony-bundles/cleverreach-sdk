<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:52
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class AttributeType
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum AttributeType: string
{
    case TEXT = 'text';
    case NUMBER = 'number';
    case GENDER = 'gender';
    case DATE = 'date';
}
