<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 22:07
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class OrderDirection
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum OrderDirection: string
{
    case ASC = 'ASC';
    case DESC = 'DESC';
}
