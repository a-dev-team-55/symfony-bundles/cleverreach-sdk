<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 18:57
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class State
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum ReportState: string
{
    case SENT = 'sent';
    case OPENED = 'opened';
    case CLICKED = 'clicked';
    case NOT_OPENED = 'notopened';
    case NOT_CLICKED = 'notclicked';
    case BOUNCED = 'bounced';
    case UNSUBSCRIBED = 'unsubscribed';
}
