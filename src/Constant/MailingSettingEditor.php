<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 19:50
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Constant;

/**
 * @class MailingSettingEditor
 * @package ADevTeam\CleverReachBundle\Constant
 */
enum MailingSettingEditor: string
{
    case WIZARD = 'wizard';
    case FREEFORM = 'freeform';
    case ADVANCED = 'advanced';
    case PLAINTEXT = 'plaintext';
}
