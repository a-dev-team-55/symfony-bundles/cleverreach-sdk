<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 17:51
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Http;

use ADevTeam\CleverReachBundle\Interface\CleverReachCountResponseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * @class CleverReachCountResponse
 * @package ADevTeam\CleverReachBundle\Http
 */
class CleverReachCountResponse extends Response implements CleverReachCountResponseInterface
{

    /**
     * @param ResponseInterface $response
     * @param string $key
     */
    public function __construct(ResponseInterface $response, string $key)
    {
        $response->getBody()->rewind();

        parent::__construct(
            $response->getStatusCode(),
            $response->getHeaders(),
            json_decode($response->getBody()->getContents(), true)[$key] ?? 0
        );
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        return json_decode($this->getBody()->getContents(), true);
    }
}
