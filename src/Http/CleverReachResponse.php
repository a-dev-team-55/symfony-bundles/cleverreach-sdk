<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 17:51
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Http;

use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use DateTime;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * @class CleverReachResponse
 * @package ADevTeam\CleverReachBundle\Http
 */
class CleverReachResponse extends Response implements CleverReachResponseInterface
{

    /**
     * @param ResponseInterface $response
     * @param RequestInterface|null $request
     * @param LoggerInterface|null $requestLogger
     * @param LoggerInterface|null $responseLogger
     */
    public function __construct(
        ResponseInterface $response,
        ?RequestInterface $request = null,
        ?LoggerInterface $requestLogger = null,
        ?LoggerInterface $responseLogger = null
    ) {
        $response->getBody()->rewind();

        parent::__construct(
            $response->getStatusCode(),
            $response->getHeaders(),
            $response->getBody()
        );

        if ($request && $requestLogger && $responseLogger) {
            $stamp = (new DateTime('now'))->getTimestamp();

            $this->logRequest($request, $stamp, $requestLogger);
            $this->logResponse($request, $stamp, $responseLogger);
        }
    }

    /**
     * @param RequestInterface $request
     * @param int $stamp
     * @param LoggerInterface $logger
     * @return void
     */
    private function logRequest(RequestInterface $request, int $stamp, LoggerInterface $logger): void
    {
        $params = json_decode($request->getBody()->getContents(), true) ?? [];

        $data = [
            'method' => $request->getMethod(),
            'params' => $params,
        ];

        $message = sprintf('%s => %s', $stamp, $request->getUri());

        $logger->debug($message, $data);
    }

    /**
     * @param RequestInterface $request
     * @param int $stamp
     * @param LoggerInterface $logger
     * @return void
     */
    private function logResponse(RequestInterface $request, int $stamp, LoggerInterface $logger): void
    {
        $data = [
            'status' => $this->getStatusCode(),
            'data' => $this->toArray()
        ];

        $message = sprintf('%s => %s', $stamp, $request->getUri());

        $logger->debug($message, $data);
    }

    /**
     * @return mixed
     */
    public function toArray(): mixed
    {
        return json_decode($this->getBody()->__toString(), true);
    }
}
