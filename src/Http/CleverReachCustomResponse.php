<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 17:51
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Http;

use ADevTeam\CleverReachBundle\Interface\CleverReachResponseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @class CleverReachResponse
 * @package ADevTeam\CleverReachBundle\Http
 */
class CleverReachCustomResponse extends Response implements CleverReachResponseInterface
{

    /**
     * @param int $statusCode
     * @param array $headers
     * @param StreamInterface $body
     */
    public function __construct(int $statusCode, array $headers, StreamInterface $body )
    {
        parent::__construct(
            $statusCode,
            $headers,
            $body
        );
    }

    /**
     * @return mixed
     */
    public function toArray(): mixed
    {
        return json_decode($this->getBody()->getContents(), true);
    }
}
