<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 17:49
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @class CleverReachBundle
 * @package ADevTeam\CleverReachBundle
 * @extends Bundle
 */
class CleverReachBundle extends Bundle
{

}
