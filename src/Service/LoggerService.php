<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 15.11.2023 17:49
 * @link https://avanhulst.de
 * @support info@avanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Service;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Level;
use Monolog\Logger;
use Monolog\Processor\PsrLogMessageProcessor;
use Psr\Log\LoggerInterface;
use ReflectionClass;

/**
 * @class LoggerService
 * @package ADevTeam\CleverReachBundle\Service
 */
class LoggerService
{

    /**
     * @param string $rotatingFilePathPattern
     * @param int $defaultFileRotationCount
     */
    public function __construct(
        private readonly string $rotatingFilePathPattern,
        private readonly int    $defaultFileRotationCount = 14
    ) {}

    /**
     * @param string $filePrefix
     * @param string $logPrefix
     * @param int|null $fileRotationCount
     * @return LoggerInterface
     */
    public function createRotating(string $filePrefix, string $logPrefix, ?int $fileRotationCount = null): LoggerInterface
    {
        $logger = new Logger($logPrefix);
        $logger->pushProcessor(new PsrLogMessageProcessor());

        $loggerLevel = $_ENV['APP_DEBUG'] == 1 ? Level::Debug : Level::Info;

        $filepath = sprintf($this->rotatingFilePathPattern, $filePrefix);
        $handler = new RotatingFileHandler($filepath, $fileRotationCount ?? $this->defaultFileRotationCount, $loggerLevel);

        $logger->setHandlers([$handler]);

        return $logger;
    }
}
