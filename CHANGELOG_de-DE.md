# 0.1.0
* Bundle Grundaufbau erstellt

# 0.1.1
* Endpunkte und Methoden hinzugefügt

# 0.1.2
* WebHook Api Client hinzugefügt
* Fehlende Eigenschaften zum ReceiverStruct hinzugefügt

# 0.1.3
* Coderefactoring
* Readme erstellt

# 1.0.2
* Abhängigkeiten angepasst

# 1.0.3
* Guzzle Streamfactory Fehler behoben
