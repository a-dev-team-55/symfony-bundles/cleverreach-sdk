<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 16:34
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Test\TestCase;

use ADevTeam\CleverReachBundle\Client\V3\CleverReachClient;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\Exception;

/**
 * @class PartialTestCase
 * @package ADevTeam\CleverReachBundle\Test
 */
class PartialTestCase extends CleverReachClientTestCase
{

    /**
     * @return void
     * @throws GuzzleException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $clientId = $_ENV['CLEVERREACH_CLIENT_ID'] ?? '';
        $clientSecret = $_ENV['CLEVERREACH_CLIENT_SECRET'] ?? '';

        $this->assertNotEmpty($clientId);
        $this->assertNotEmpty($clientSecret);

        $this->client
            ->setClientId($clientId)
            ->setClientSecret($clientSecret)
            ->authenticate();
    }
}
