<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 26.11.2023 16:34
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Test\TestCase;

use ADevTeam\CleverReachBundle\Client\V3\CleverReachClient;
use ADevTeam\CleverReachBundle\Test\AppKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @class CleverReachClientTestCase
 * @package ADevTeam\CleverReachBundle\Test
 */
class CleverReachClientTestCase extends KernelTestCase
{

    /**
     * @var ContainerInterface $container
     */
    protected ContainerInterface $container;

    /**
     * @var CleverReachClient|null $client
     */
    protected ?CleverReachClient $client;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $kernel = new AppKernel('test', true);
        $kernel->boot();
        $this->container = $kernel->getContainer();

        $this->client = $this->container->get(CleverReachClient::class);
    }
}
