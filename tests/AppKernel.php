<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 25.11.2023 13:56
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Test;

use ADevTeam\CleverReachBundle\CleverReachBundle;
use Exception;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpKernel\Kernel;

/**
 * @class AppKernel
 * @package ADevTeam\CleverReachBundle\Tests
 * @extends Kernel
 */
class AppKernel extends Kernel
{

    /**
     * @return void
     */
    public function boot(): void
    {
        parent::boot();

        $dotenv = new Dotenv();
        $dotenv->load(__DIR__.'/../.env.test');
        $dotenv->overload(__DIR__.'/../.env.test');
        $dotenv->loadEnv(__DIR__.'/../.env.test');
    }

    /**
     * @return array
     */
    public function registerBundles(): array
    {
        $bundles = array();

        if ($this->getEnvironment() == 'test') {
            $bundles[] = new FrameworkBundle();
            $bundles[] = new CleverReachBundle();
        }

        return $bundles;
    }

    /**
     * @param LoaderInterface $loader
     * @throws Exception
     */
    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(__DIR__.'/../config/services.yaml');
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheDir(): string
    {
        return sys_get_temp_dir().'/CleverReachBundle/cache';
    }

    /**
     * {@inheritdoc}
     */
    public function getLogDir(): string
    {
        return sys_get_temp_dir().'/CleverReachBundle/logs';
    }
}
