<?php declare(strict_types=1);

namespace ADevTeam\CleverReachBundle\Test\Functional;

use ADevTeam\CleverReachBundle\Client\V3\Partial\GroupPartial;
use ADevTeam\CleverReachBundle\Struct\GroupStruct;
use ADevTeam\CleverReachBundle\Test\TestCase\PartialTestCase;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\HttpFoundation\Response;

/**
 * @test
 */
class GroupPartialTest extends PartialTestCase
{

    private const GROUP_NAME = 'PhpUnit Testgroup';
    private const FORM_NAME = 'PhpUnit Testform';

    /**
     * @test
     * @return void
     * @throws GuzzleException
     */
    #[NoReturn] public function clean(): void
    {
        $response = $this->client->groups()->getAll();
        $groups = $response->toArray();

        foreach ($groups as $group) {
            if ($group['name'] === self::GROUP_NAME) {
                $groupId = $group['id'];

                $response = $this->client->groups()->delete($groupId);
                $state = $response->toArray();

                $this->assertIsBool($state);
                $this->assertTrue($state);
            }
        }
    }

    /**
     * @test
     * @depends clean
     * @return string
     * @throws GuzzleException
     */
    public function addGroup(): string
    {
        $group = new GroupStruct(self::GROUP_NAME);
        $response = $this->client->groups()->create($group);
        $data = $response->toArray();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertIsArray($data);

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('locked', $data);
        $this->assertArrayHasKey('backup', $data);
        $this->assertArrayHasKey('receiver_info', $data);
        $this->assertArrayHasKey('stamp', $data);
        $this->assertArrayHasKey('last_mailing', $data);
        $this->assertArrayHasKey('last_changed', $data);

        $this->assertEquals(self::GROUP_NAME, $data['name']);
        $this->assertEquals(0, $data['last_mailing']);
        $this->assertFalse($data['locked']);
        $this->assertFalse($data['backup']);
        $this->assertIsString($data['id']);
        $this->assertNotEmpty($data['id']);

        return $data['id'];
    }

    /**
     * @test
     * @depends addGroup
     * @throws GuzzleException
     */
    #[NoReturn] public function deleteGroup(string $groupId): void
    {
        $this->assertNotNull($groupId);

        $response = $this->client->groups()->delete($groupId);
        $state = $response->toArray();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertIsBool($state);
        $this->assertTrue($state);
    }
}
