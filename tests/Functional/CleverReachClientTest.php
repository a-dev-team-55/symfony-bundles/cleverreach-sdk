<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 25.11.2023 14:06
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Test\Functional;

use ADevTeam\CleverReachBundle\Client\V3\CleverReachClient;
use ADevTeam\CleverReachBundle\Client\V3\Partial\FormPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\GroupPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\HookPartial;
use ADevTeam\CleverReachBundle\Client\V3\Partial\ReceiverPartial;
use ADevTeam\CleverReachBundle\Test\TestCase\CleverReachClientTestCase;

/**
 * @test
 */
class CleverReachClientTest extends CleverReachClientTestCase
{

//    /**
//     * @test
//     */
//    public function serviceExists()
//    {
//        $this->assertInstanceOf(CleverReachClient::class, $this->client);
//    }
//
//    /**
//     * @test
//     * @depends serviceExists
//     */
//    public function authenticate()
//    {
//        $clientId = $_ENV['CLEVERREACH_CLIENT_ID'] ?? '';
//        $clientSecret = $_ENV['CLEVERREACH_CLIENT_SECRET'] ?? '';
//
//        $this->assertNotEmpty($clientId);
//        $this->assertNotEmpty($clientSecret);
//
//        $authenticated = $this->client
//            ->setClientId($clientId)
//            ->setClientSecret($clientSecret)
//            ->authenticate();
//
//        $this->assertTrue($authenticated);
//    }
//
//    /**
//     * @test
//     * @depends authenticate
//     */
//    public function formPartialExists()
//    {
//        $this->assertInstanceOf(FormPartial::class, $this->client->forms());
//    }
//
//    /**
//     * @test
//     * @depends formPartialExists
//     */
//    public function hookPartialExists()
//    {
//        $this->assertInstanceOf(HookPartial::class, $this->client->hooks());
//    }
//
//    /**
//     * @test
//     * @depends hookPartialExists
//     */
//    public function groupPartialExists()
//    {
//        $this->assertInstanceOf(GroupPartial::class, $this->client->groups());
//    }
//
//    /**
//     * @test
//     * @depends groupPartialExists
//     */
//    public function receiverPartialExists()
//    {
//        $this->assertInstanceOf(ReceiverPartial::class, $this->client->receivers());
//    }
}
