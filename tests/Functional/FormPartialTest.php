<?php declare(strict_types=1);
/*
 * @author ADevTeam
 * @created 27.11.2023 14:18
 * @link https://avanhulst.de
 * @support andreas@vanhulst.de
 * @license MIT
 *
 * @copyright 2023 © ADevTeam
 */

namespace ADevTeam\CleverReachBundle\Test\Functional;

use ADevTeam\CleverReachBundle\Client\V3\Partial\GroupPartial;
use ADevTeam\CleverReachBundle\Struct\GroupStruct;
use ADevTeam\CleverReachBundle\Test\TestCase\PartialTestCase;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\NoReturn;

/**
 * @test
 */
class FormPartialTest extends PartialTestCase
{

//    /**
//     * @test
//     * @throws GuzzleException
//     */
//    #[NoReturn] public function getAll(): void
//    {
//        $response = $this->client->groups()->getStats('553184');

//        $data = json_decode($response->getContent(), true);

//        dd($response->toArray());
//    }
}
